import { Inject, Injectable } from '@nestjs/common';
import { ConflictingOrderException } from '../../common/exceptions/conflicting-order.exception';
import { TodoEntity } from './entities/todo.entity';
import {
  ITodoRepository,
  TODO_REPOSITORY_TOKEN,
} from './interfaces/todo-repository.interface';
import { ITodoService } from './interfaces/todo-service.interface';
import { TodoPatch } from './types/todo.type';

@Injectable()
export class TodoService implements ITodoService {
  constructor(
    @Inject(TODO_REPOSITORY_TOKEN)
    private readonly todoRepository: ITodoRepository,
  ) {}

  async create(title: string): Promise<TodoEntity> {
    const order = (await this.todoRepository.getLastOrder()) + 1;
    const todoToSave = new TodoEntity(title, order, false);
    return this.todoRepository.create(todoToSave);
  }

  async delete(id: string): Promise<TodoEntity | undefined> {
    return this.todoRepository.delete(id);
  }

  async deleteAll(completed: boolean): Promise<void> {
    return this.todoRepository.deleteAll(completed);
  }

  async fetch(id: string): Promise<TodoEntity> {
    return this.todoRepository.fetch(id);
  }

  async fetchAll(): Promise<TodoEntity[]> {
    const todos = await this.todoRepository.fetchAll();
    return todos.sort((a, b) => a.order - b.order);
  }

  async patch(id: string, patch: TodoPatch): Promise<TodoEntity> {
    if (patch.order) {
      const todo = await this.todoRepository.fetchByOrder(patch.order);
      if (todo && todo.id !== id) {
        throw new ConflictingOrderException(
          `Conflicting order occurred, order:${patch.order} already exists`,
        );
      }
    }
    return this.todoRepository.patch(id, patch);
  }
}
