import { Transform, Type } from 'class-transformer';
import {
  IsBoolean,
  IsNumber,
  IsOptional,
  IsString,
  Min,
} from 'class-validator';
import { IsNotBlank } from 'src/common/decorators/validations/isNotBlank';
import { TodoEntity } from '../entities/todo.entity';

export class TodoCreateDto {
  @IsNotBlank()
  @IsString()
  title: string;
}

export class TodoDeleteQueryDto {
  @Transform(({ value }) => {
    if (typeof value !== 'boolean' && value !== 'true' && value !== 'false') {
      return -1;
    }
    return [true, 'true'].indexOf(value) > -1;
  })
  @IsBoolean()
  @IsOptional()
  completed: boolean;
}

export class TodoPatchDto {
  @IsNotBlank()
  @IsString()
  @IsOptional()
  title: string;

  @Type(() => Number)
  @Min(1)
  @IsNumber()
  @IsOptional()
  order: number;

  @Transform(({ value }) => {
    if (typeof value !== 'boolean' && value !== 'true' && value !== 'false') {
      return -1;
    }
    return [true, 'true'].indexOf(value) > -1;
  })
  @IsBoolean()
  @IsOptional()
  completed: boolean;
}

export class TodoUpdateDto {
  @IsNotBlank()
  @IsString()
  title: string;

  @Type(() => Number)
  @Min(1)
  @IsNumber()
  order: number;

  @Transform(({ value }) => {
    if (typeof value !== 'boolean' && value !== 'true' && value !== 'false') {
      return -1;
    }
    return [true, 'true'].indexOf(value) > -1;
  })
  @IsBoolean()
  completed: boolean;
}

export type TodoResponseDto = TodoEntity & { url: string };
