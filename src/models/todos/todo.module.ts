import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TODO_ENPOINT } from 'src/common/constants/endpoints';
import { UrlHelper } from 'src/common/helpers/url.helper';
import {
  UuidGenerator,
  UUID_GENERATOR_TOKEN,
} from 'src/common/helpers/uuid-generator.helper';
import { UriMiddleware } from 'src/common/middlewares/uri.middleware';
import { AppConfigService } from 'src/config/app/config.service';
import { TodoEntity } from './entities/todo.entity';
import { InMemoryTodoRepository } from './in-memory-todo.repository';
import {
  ITodoRepository,
  TODO_REPOSITORY_TOKEN,
} from './interfaces/todo-repository.interface';
import { TODO_SERVICE_TOKEN } from './interfaces/todo-service.interface';
import { TodoController } from './todo.controller';
import { TodoService } from './todo.service';
import { TypeOrmTodoRepository } from './typeorm-todo.repository';

const repositoryFactory = (
  appConfigService: AppConfigService,
  inMemoryTodoRepository: ITodoRepository,
  typeOrmTodoRepository: ITodoRepository,
) => (appConfigService.isDev ? inMemoryTodoRepository : typeOrmTodoRepository);

@Module({
  imports: [TypeOrmModule.forFeature([TodoEntity])],
  controllers: [TodoController],
  providers: [
    InMemoryTodoRepository,
    TypeOrmTodoRepository,
    TodoService,
    UuidGenerator,
    UrlHelper,
    {
      provide: TODO_REPOSITORY_TOKEN,
      useFactory: repositoryFactory,
      inject: [AppConfigService, InMemoryTodoRepository, TypeOrmTodoRepository],
    },
    {
      provide: UUID_GENERATOR_TOKEN,
      useClass: UuidGenerator,
    },
    {
      provide: TODO_SERVICE_TOKEN,
      useClass: TodoService,
    },
  ],
})
export class TodoModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(UriMiddleware).forRoutes(TODO_ENPOINT);
  }
}
