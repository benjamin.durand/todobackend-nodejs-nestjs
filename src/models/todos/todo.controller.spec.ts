import { INestApplication, ValidationPipe } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { UrlHelper } from 'src/common/helpers/url.helper';
import { MockOf } from 'src/common/mocks/mock-of';
import * as request from 'supertest';
import { ConflictingOrderException } from '../../common/exceptions/conflicting-order.exception';
import { TodoEntity } from './entities/todo.entity';
import {
  ITodoService,
  TODO_SERVICE_TOKEN,
} from './interfaces/todo-service.interface';
import { TodoController } from './todo.controller';

describe('TodoController', () => {
  let app: INestApplication;
  let service: MockOf<ITodoService>;

  const URL = 'http://localhost/todos/1';

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      controllers: [TodoController],
      providers: [
        {
          provide: TODO_SERVICE_TOKEN,
          useValue: {
            create: jest.fn(),
            delete: jest.fn(),
            deleteAll: jest.fn(),
            fetch: jest.fn(),
            fetchAll: jest.fn(),
            patch: jest.fn(),
          },
        },
        {
          provide: UrlHelper,
          useValue: { generate: () => URL },
        },
      ],
    }).compile();

    app = moduleRef.createNestApplication();
    app.useLogger(false);
    app.useGlobalPipes(new ValidationPipe());
    await app.init();

    service = moduleRef.get<MockOf<ITodoService>>(TODO_SERVICE_TOKEN);
  });

  afterAll(async () => {
    await app.close();
  });

  describe('addTodo', () => {
    it('should create todo', async () => {
      const todo: TodoEntity = {
        id: '1',
        title: 'a title',
        order: 1,
        completed: false,
      };
      service.create.mockReturnValue(Promise.resolve(todo));

      await request(app.getHttpServer())
        .post('/todos')
        .send({ title: 'a title' })
        .expect(201)
        .expect((res: Response) => {
          expect(res.body).toEqual({
            id: '1',
            title: 'a title',
            completed: false,
            order: 1,
            url: URL,
          });
        });

      expect(service.create).toHaveBeenCalledTimes(1);
      expect(service.create).toHaveBeenCalledWith('a title');

      expect(service.delete).not.toHaveBeenCalled();
      expect(service.deleteAll).not.toHaveBeenCalled();
      expect(service.fetch).not.toHaveBeenCalled();
      expect(service.fetchAll).not.toHaveBeenCalled();
      expect(service.patch).not.toHaveBeenCalled();
    });

    it.each`
      title
      ${null}
      ${undefined}
      ${1}
      ${true}
      ${{}}
      ${[]}
    `(
      'should send bad request when title is not a string',
      async ({ title }: { title: string }) => {
        await request(app.getHttpServer())
          .post('/todos')
          .send({ title })
          .expect(400)
          .expect((res: Response) => {
            expect(res.body).toEqual({
              error: 'Bad Request',
              message: ['title must be a string', 'title must not be blank'],
              statusCode: 400,
            });
          });

        expect(service.create).not.toHaveBeenCalled();
        expect(service.delete).not.toHaveBeenCalled();
        expect(service.deleteAll).not.toHaveBeenCalled();
        expect(service.fetch).not.toHaveBeenCalled();
        expect(service.fetchAll).not.toHaveBeenCalled();
        expect(service.patch).not.toHaveBeenCalled();
      },
    );

    it.each`
      title
      ${''}
      ${' '}
    `(
      'should send bad request when title is blank',
      async ({ title }: { title: string }) => {
        await request(app.getHttpServer())
          .post('/todos')
          .send({ title })
          .expect(400)
          .expect((res: Response) => {
            expect(res.body).toEqual({
              error: 'Bad Request',
              message: ['title must not be blank'],
              statusCode: 400,
            });
          });

        expect(service.create).not.toHaveBeenCalled();
        expect(service.delete).not.toHaveBeenCalled();
        expect(service.deleteAll).not.toHaveBeenCalled();
        expect(service.fetch).not.toHaveBeenCalled();
        expect(service.fetchAll).not.toHaveBeenCalled();
        expect(service.patch).not.toHaveBeenCalled();
      },
    );

    it('should send internal server error when service rejects', async () => {
      const error = new Error('An error occurred');
      service.create.mockRejectedValue(error);

      await request(app.getHttpServer())
        .post('/todos')
        .send({ title: 'a title' })
        .expect(500)
        .expect((res: Response) => {
          expect(res.body).toEqual({
            message: 'Internal server error',
            statusCode: 500,
          });
        });

      expect(service.create).toHaveBeenCalledTimes(1);
      expect(service.create).toHaveBeenCalledWith('a title');

      expect(service.delete).not.toHaveBeenCalled();
      expect(service.deleteAll).not.toHaveBeenCalled();
      expect(service.fetch).not.toHaveBeenCalled();
      expect(service.fetchAll).not.toHaveBeenCalled();
      expect(service.patch).not.toHaveBeenCalled();
    });
  });

  describe('deleteTodo', () => {
    it('should send no content', async () => {
      const todo: TodoEntity = {
        id: '1',
        title: 'a title',
        order: 1,
        completed: false,
      };
      service.delete.mockReturnValue(Promise.resolve(todo));

      await request(app.getHttpServer())
        .delete('/todos/1')
        .send()
        .expect(204)
        .expect((res: Response) => {
          expect(res.body).toEqual({});
        });

      expect(service.delete).toHaveBeenCalledTimes(1);
      expect(service.delete).toHaveBeenCalledWith('1');

      expect(service.create).not.toHaveBeenCalled();
      expect(service.deleteAll).not.toHaveBeenCalled();
      expect(service.fetch).not.toHaveBeenCalled();
      expect(service.fetchAll).not.toHaveBeenCalled();
      expect(service.patch).not.toHaveBeenCalled();
    });

    it('should send not found', async () => {
      service.delete.mockReturnValue(Promise.resolve(undefined));

      await request(app.getHttpServer())
        .delete('/todos/1')
        .send()
        .expect(404)
        .expect((res: Response) => {
          expect(res.body).toEqual({
            message: 'Not Found',
            statusCode: 404,
          });
        });

      expect(service.delete).toHaveBeenCalledTimes(1);
      expect(service.delete).toHaveBeenCalledWith('1');

      expect(service.create).not.toHaveBeenCalled();
      expect(service.deleteAll).not.toHaveBeenCalled();
      expect(service.fetch).not.toHaveBeenCalled();
      expect(service.fetchAll).not.toHaveBeenCalled();
      expect(service.patch).not.toHaveBeenCalled();
    });

    it('should send internal server error when service rejects', async () => {
      const error = new Error('An error occurred');
      service.delete.mockRejectedValue(error);

      await request(app.getHttpServer())
        .delete('/todos/1')
        .send()
        .expect(500)
        .expect((res: Response) => {
          expect(res.body).toEqual({
            message: 'Internal server error',
            statusCode: 500,
          });
        });

      expect(service.delete).toHaveBeenCalledTimes(1);
      expect(service.delete).toHaveBeenCalledWith('1');

      expect(service.create).not.toHaveBeenCalled();
      expect(service.deleteAll).not.toHaveBeenCalled();
      expect(service.fetch).not.toHaveBeenCalled();
      expect(service.fetchAll).not.toHaveBeenCalled();
      expect(service.patch).not.toHaveBeenCalled();
    });
  });

  describe('deleteTodos', () => {
    it('should send no content', async () => {
      service.deleteAll.mockReturnValue(Promise.resolve());

      await request(app.getHttpServer())
        .delete('/todos')
        .send()
        .expect(204)
        .expect((res: Response) => {
          expect(res.body).toEqual({});
        });

      expect(service.deleteAll).toHaveBeenCalledTimes(1);
      expect(service.deleteAll).toHaveBeenCalledWith(false);

      expect(service.create).not.toHaveBeenCalled();
      expect(service.delete).not.toHaveBeenCalled();
      expect(service.fetch).not.toHaveBeenCalled();
      expect(service.fetchAll).not.toHaveBeenCalled();
      expect(service.patch).not.toHaveBeenCalled();
    });

    it.each`
      completed
      ${false}
      ${true}
    `(
      'should send no content',
      async ({ completed }: { completed: boolean }) => {
        service.deleteAll.mockReturnValue(Promise.resolve());

        await request(app.getHttpServer())
          .delete('/todos')
          .query({ completed })
          .send()
          .expect(204)
          .expect((res: Response) => {
            expect(res.body).toEqual({});
          });

        expect(service.deleteAll).toHaveBeenCalledTimes(1);
        expect(service.deleteAll).toHaveBeenCalledWith(completed);

        expect(service.create).not.toHaveBeenCalled();
        expect(service.delete).not.toHaveBeenCalled();
        expect(service.fetch).not.toHaveBeenCalled();
        expect(service.fetchAll).not.toHaveBeenCalled();
        expect(service.patch).not.toHaveBeenCalled();
      },
    );

    it.each`
      completed
      ${null}
      ${''}
      ${-1}
      ${0}
    `(
      'should send bad request when invalid query',
      async ({ completed }: { completed: boolean }) => {
        await request(app.getHttpServer())
          .delete('/todos')
          .query({ completed })
          .send()
          .expect(400)
          .expect((res: Response) => {
            expect(res.body).toEqual({
              error: 'Bad Request',
              message: ['completed must be a boolean value'],
              statusCode: 400,
            });
          });

        expect(service.create).not.toHaveBeenCalled();
        expect(service.delete).not.toHaveBeenCalled();
        expect(service.deleteAll).not.toHaveBeenCalled();
        expect(service.fetch).not.toHaveBeenCalled();
        expect(service.fetchAll).not.toHaveBeenCalled();
        expect(service.patch).not.toHaveBeenCalled();
      },
    );

    it('should send internal server error when service rejects', async () => {
      const error = new Error('An error occurred');
      service.deleteAll.mockRejectedValue(error);

      await request(app.getHttpServer())
        .delete('/todos')
        .send()
        .expect(500)
        .expect((res: Response) => {
          expect(res.body).toEqual({
            message: 'Internal server error',
            statusCode: 500,
          });
        });

      expect(service.deleteAll).toHaveBeenCalledTimes(1);
      expect(service.deleteAll).toHaveBeenCalledWith(false);

      expect(service.create).not.toHaveBeenCalled();
      expect(service.delete).not.toHaveBeenCalled();
      expect(service.fetch).not.toHaveBeenCalled();
      expect(service.fetchAll).not.toHaveBeenCalled();
      expect(service.patch).not.toHaveBeenCalled();
    });
  });

  describe('getTodo', () => {
    it('should return a fetched todo', async () => {
      const todo: TodoEntity = {
        id: '1',
        title: 'a title',
        order: 1,
        completed: false,
      };
      service.fetch.mockReturnValue(Promise.resolve(todo));

      await request(app.getHttpServer())
        .get('/todos/1')
        .send()
        .expect(200)
        .expect((res: Response) => {
          expect(res.body).toEqual({
            id: '1',
            title: 'a title',
            completed: false,
            order: 1,
            url: URL,
          });
        });

      expect(service.fetch).toHaveBeenCalledTimes(1);
      expect(service.fetch).toHaveBeenCalledWith('1');

      expect(service.create).not.toHaveBeenCalled();
      expect(service.delete).not.toHaveBeenCalled();
      expect(service.deleteAll).not.toHaveBeenCalled();
      expect(service.fetchAll).not.toHaveBeenCalled();
      expect(service.patch).not.toHaveBeenCalled();
    });

    it('should send not found', async () => {
      service.fetch.mockReturnValue(Promise.resolve(undefined));

      await request(app.getHttpServer())
        .get('/todos/1')
        .send()
        .expect(404)
        .expect((res: Response) => {
          expect(res.body).toEqual({
            message: 'Not Found',
            statusCode: 404,
          });
        });

      expect(service.fetch).toHaveBeenCalledTimes(1);
      expect(service.fetch).toHaveBeenCalledWith('1');

      expect(service.create).not.toHaveBeenCalled();
      expect(service.delete).not.toHaveBeenCalled();
      expect(service.deleteAll).not.toHaveBeenCalled();
      expect(service.fetchAll).not.toHaveBeenCalled();
      expect(service.patch).not.toHaveBeenCalled();
    });

    it('should send internal server error when service rejects', async () => {
      const error = new Error('An error occurred');
      service.fetch.mockRejectedValue(error);

      await request(app.getHttpServer())
        .get('/todos/1')
        .send()
        .expect(500)
        .expect((res: Response) => {
          expect(res.body).toEqual({
            message: 'Internal server error',
            statusCode: 500,
          });
        });

      expect(service.fetch).toHaveBeenCalledTimes(1);
      expect(service.fetch).toHaveBeenCalledWith('1');

      expect(service.create).not.toHaveBeenCalled();
      expect(service.delete).not.toHaveBeenCalled();
      expect(service.deleteAll).not.toHaveBeenCalled();
      expect(service.fetchAll).not.toHaveBeenCalled();
      expect(service.patch).not.toHaveBeenCalled();
    });
  });

  describe('getTodos', () => {
    it('should return fetched todos', async () => {
      const todos: TodoEntity[] = [
        {
          id: '1',
          title: 'a title',
          order: 1,
          completed: false,
        },
        {
          id: '2',
          title: 'another title',
          order: 2,
          completed: false,
        },
      ];
      service.fetchAll.mockReturnValue(Promise.resolve(todos));

      await request(app.getHttpServer())
        .get('/todos')
        .send()
        .expect(200)
        .expect((res: Response) => {
          expect(res.body).toEqual([
            {
              id: '1',
              title: 'a title',
              completed: false,
              order: 1,
              url: URL,
            },
            {
              id: '2',
              title: 'another title',
              completed: false,
              order: 2,
              url: URL,
            },
          ]);
        });

      expect(service.fetchAll).toHaveBeenCalledTimes(1);
      expect(service.fetchAll).toHaveBeenCalledWith();

      expect(service.create).not.toHaveBeenCalled();
      expect(service.delete).not.toHaveBeenCalled();
      expect(service.deleteAll).not.toHaveBeenCalled();
      expect(service.fetch).not.toHaveBeenCalled();
      expect(service.patch).not.toHaveBeenCalled();
    });

    it('should send internal server error when service rejects', async () => {
      const error = new Error('An error occurred');
      service.fetchAll.mockRejectedValue(error);

      await request(app.getHttpServer())
        .get('/todos')
        .send()
        .expect(500)
        .expect((res: Response) => {
          expect(res.body).toEqual({
            message: 'Internal server error',
            statusCode: 500,
          });
        });

      expect(service.fetchAll).toHaveBeenCalledTimes(1);
      expect(service.fetchAll).toHaveBeenCalledWith();

      expect(service.create).not.toHaveBeenCalled();
      expect(service.delete).not.toHaveBeenCalled();
      expect(service.deleteAll).not.toHaveBeenCalled();
      expect(service.fetch).not.toHaveBeenCalled();
      expect(service.patch).not.toHaveBeenCalled();
    });
  });

  describe('patchTodo', () => {
    it('should return patched todo', async () => {
      const todo: TodoEntity = {
        id: '1',
        title: 'a title',
        order: 1,
        completed: false,
      };
      service.patch.mockReturnValue(Promise.resolve(todo));

      const body = {
        title: 'a title',
        order: 1,
        completed: false,
      };
      await request(app.getHttpServer())
        .patch('/todos/1')
        .send(body)
        .expect(200)
        .expect((res: Response) => {
          expect(res.body).toEqual({
            id: '1',
            title: 'a title',
            completed: false,
            order: 1,
            url: URL,
          });
        });

      expect(service.patch).toHaveBeenCalledTimes(1);
      expect(service.patch).toHaveBeenCalledWith('1', body);

      expect(service.create).not.toHaveBeenCalled();
      expect(service.delete).not.toHaveBeenCalled();
      expect(service.deleteAll).not.toHaveBeenCalled();
      expect(service.fetch).not.toHaveBeenCalled();
      expect(service.fetchAll).not.toHaveBeenCalled();
    });

    it('should return patched todo with plain body', async () => {
      const todo: TodoEntity = {
        id: '1',
        title: 'a title',
        order: 1,
        completed: false,
      };
      service.patch.mockReturnValue(Promise.resolve(todo));

      const body = {};
      await request(app.getHttpServer())
        .patch('/todos/1')
        .send(body)
        .expect(200)
        .expect((res: Response) => {
          expect(res.body).toEqual({
            id: '1',
            title: 'a title',
            completed: false,
            order: 1,
            url: URL,
          });
        });

      expect(service.patch).toHaveBeenCalledTimes(1);
      expect(service.patch).toHaveBeenCalledWith('1', body);

      expect(service.create).not.toHaveBeenCalled();
      expect(service.delete).not.toHaveBeenCalled();
      expect(service.deleteAll).not.toHaveBeenCalled();
      expect(service.fetch).not.toHaveBeenCalled();
      expect(service.fetchAll).not.toHaveBeenCalled();
    });

    it('should send not found when no todo', async () => {
      service.patch.mockReturnValue(Promise.resolve(undefined));

      const body = {
        title: 'a title',
        order: 1,
        completed: false,
      };
      await request(app.getHttpServer())
        .patch('/todos/1')
        .send(body)
        .expect(404)
        .expect((res: Response) => {
          expect(res.body).toEqual({
            message: 'Not Found',
            statusCode: 404,
          });
        });

      expect(service.patch).toHaveBeenCalledTimes(1);
      expect(service.patch).toHaveBeenCalledWith('1', body);

      expect(service.create).not.toHaveBeenCalled();
      expect(service.delete).not.toHaveBeenCalled();
      expect(service.deleteAll).not.toHaveBeenCalled();
      expect(service.fetch).not.toHaveBeenCalled();
      expect(service.fetchAll).not.toHaveBeenCalled();
    });

    it.each`
      title
      ${1}
      ${true}
      ${{}}
      ${[]}
    `(
      'should send bad request when title is not a string',
      async ({ title }: { title: string }) => {
        await request(app.getHttpServer())
          .patch('/todos/1')
          .send({ title })
          .expect(400)
          .expect((res: Response) => {
            expect(res.body).toEqual({
              error: 'Bad Request',
              message: ['title must be a string', 'title must not be blank'],
              statusCode: 400,
            });
          });

        expect(service.create).not.toHaveBeenCalled();
        expect(service.delete).not.toHaveBeenCalled();
        expect(service.deleteAll).not.toHaveBeenCalled();
        expect(service.fetch).not.toHaveBeenCalled();
        expect(service.fetchAll).not.toHaveBeenCalled();
        expect(service.patch).not.toHaveBeenCalled();
      },
    );

    it.each`
      title
      ${''}
      ${' '}
    `(
      'should send bad request when title is blank',
      async ({ title }: { title: string }) => {
        await request(app.getHttpServer())
          .patch('/todos/1')
          .send({ title })
          .expect(400)
          .expect((res: Response) => {
            expect(res.body).toEqual({
              error: 'Bad Request',
              message: ['title must not be blank'],
              statusCode: 400,
            });
          });

        expect(service.create).not.toHaveBeenCalled();
        expect(service.delete).not.toHaveBeenCalled();
        expect(service.deleteAll).not.toHaveBeenCalled();
        expect(service.fetch).not.toHaveBeenCalled();
        expect(service.fetchAll).not.toHaveBeenCalled();
        expect(service.patch).not.toHaveBeenCalled();
      },
    );

    it.each`
      order
      ${{}}
      ${[]}
    `(
      'should send bad request when invalid order',
      async ({ order }: { order: number }) => {
        await request(app.getHttpServer())
          .patch('/todos/1')
          .send({ order })
          .expect(400)
          .expect((res: Response) => {
            expect(res.body).toEqual({
              error: 'Bad Request',
              message: [
                'order must be a number conforming to the specified constraints',
                'order must not be less than 1',
              ],
              statusCode: 400,
            });
          });

        expect(service.create).not.toHaveBeenCalled();
        expect(service.delete).not.toHaveBeenCalled();
        expect(service.deleteAll).not.toHaveBeenCalled();
        expect(service.fetch).not.toHaveBeenCalled();
        expect(service.fetchAll).not.toHaveBeenCalled();
        expect(service.patch).not.toHaveBeenCalled();
      },
    );

    it.each`
      order
      ${''}
      ${-1}
      ${0}
    `(
      'should send bad request when invalid order',
      async ({ order }: { order: number }) => {
        await request(app.getHttpServer())
          .patch('/todos/1')
          .send({ order })
          .expect(400)
          .expect((res: Response) => {
            expect(res.body).toEqual({
              error: 'Bad Request',
              message: ['order must not be less than 1'],
              statusCode: 400,
            });
          });

        expect(service.create).not.toHaveBeenCalled();
        expect(service.delete).not.toHaveBeenCalled();
        expect(service.deleteAll).not.toHaveBeenCalled();
        expect(service.fetch).not.toHaveBeenCalled();
        expect(service.fetchAll).not.toHaveBeenCalled();
        expect(service.patch).not.toHaveBeenCalled();
      },
    );

    it.each`
      completed
      ${null}
      ${''}
      ${-1}
      ${0}
    `(
      'should send bad request when invalid order',
      async ({ completed }: { completed: boolean }) => {
        await request(app.getHttpServer())
          .patch('/todos/1')
          .send({ completed })
          .expect(400)
          .expect((res: Response) => {
            expect(res.body).toEqual({
              error: 'Bad Request',
              message: ['completed must be a boolean value'],
              statusCode: 400,
            });
          });

        expect(service.create).not.toHaveBeenCalled();
        expect(service.delete).not.toHaveBeenCalled();
        expect(service.deleteAll).not.toHaveBeenCalled();
        expect(service.fetch).not.toHaveBeenCalled();
        expect(service.fetchAll).not.toHaveBeenCalled();
        expect(service.patch).not.toHaveBeenCalled();
      },
    );

    it('should send conflict when conflicting order exception thrown', async () => {
      const error = new ConflictingOrderException('Conflicting order occurred');
      service.patch.mockRejectedValue(error);

      const body = {
        title: 'a title',
        order: 1,
        completed: false,
      };
      await request(app.getHttpServer())
        .patch('/todos/1')
        .send(body)
        .expect(409)
        .expect((res: Response) => {
          expect(res.body).toEqual({
            message: 'Conflicting order occurred',
            statusCode: 409,
          });
        });

      expect(service.patch).toHaveBeenCalledTimes(1);
      expect(service.patch).toHaveBeenCalledWith('1', body);

      expect(service.create).not.toHaveBeenCalled();
      expect(service.delete).not.toHaveBeenCalled();
      expect(service.deleteAll).not.toHaveBeenCalled();
      expect(service.fetch).not.toHaveBeenCalled();
      expect(service.fetchAll).not.toHaveBeenCalled();
    });

    it('should send internal server error when service rejects', async () => {
      const error = new Error('An error occurred');
      service.patch.mockRejectedValue(error);

      const body = {
        title: 'a title',
        order: 1,
        completed: false,
      };
      await request(app.getHttpServer())
        .patch('/todos/1')
        .send(body)
        .expect(500)
        .expect((res: Response) => {
          expect(res.body).toEqual({
            message: 'Internal server error',
            statusCode: 500,
          });
        });

      expect(service.patch).toHaveBeenCalledTimes(1);
      expect(service.patch).toHaveBeenCalledWith('1', body);

      expect(service.create).not.toHaveBeenCalled();
      expect(service.delete).not.toHaveBeenCalled();
      expect(service.deleteAll).not.toHaveBeenCalled();
      expect(service.fetch).not.toHaveBeenCalled();
      expect(service.fetchAll).not.toHaveBeenCalled();
    });
  });

  describe('updateTodo', () => {
    it('should return updated todo', async () => {
      const todo: TodoEntity = {
        id: '1',
        title: 'a title',
        order: 1,
        completed: false,
      };
      service.patch.mockReturnValue(Promise.resolve(todo));

      const body = {
        title: 'a title',
        order: 1,
        completed: false,
      };
      await request(app.getHttpServer())
        .put('/todos/1')
        .send(body)
        .expect(200)
        .expect((res: Response) => {
          expect(res.body).toEqual({
            id: '1',
            title: 'a title',
            completed: false,
            order: 1,
            url: URL,
          });
        });

      expect(service.patch).toHaveBeenCalledTimes(1);
      expect(service.patch).toHaveBeenCalledWith('1', body);

      expect(service.create).not.toHaveBeenCalled();
      expect(service.delete).not.toHaveBeenCalled();
      expect(service.deleteAll).not.toHaveBeenCalled();
      expect(service.fetch).not.toHaveBeenCalled();
      expect(service.fetchAll).not.toHaveBeenCalled();
    });

    it('should send not found when no todo', async () => {
      service.patch.mockReturnValue(Promise.resolve(undefined));

      const body = {
        title: 'a title',
        order: 1,
        completed: false,
      };
      await request(app.getHttpServer())
        .put('/todos/1')
        .send(body)
        .expect(404)
        .expect((res: Response) => {
          expect(res.body).toEqual({
            message: 'Not Found',
            statusCode: 404,
          });
        });

      expect(service.patch).toHaveBeenCalledTimes(1);
      expect(service.patch).toHaveBeenCalledWith('1', body);

      expect(service.create).not.toHaveBeenCalled();
      expect(service.delete).not.toHaveBeenCalled();
      expect(service.deleteAll).not.toHaveBeenCalled();
      expect(service.fetch).not.toHaveBeenCalled();
      expect(service.fetchAll).not.toHaveBeenCalled();
    });

    it.each`
      title
      ${1}
      ${true}
      ${{}}
      ${[]}
    `(
      'should send bad request when title is not a string',
      async ({ title }: { title: string }) => {
        await request(app.getHttpServer())
          .put('/todos/1')
          .send({ title, order: 1, completed: false })
          .expect(400)
          .expect((res: Response) => {
            expect(res.body).toEqual({
              error: 'Bad Request',
              message: ['title must be a string', 'title must not be blank'],
              statusCode: 400,
            });
          });

        expect(service.create).not.toHaveBeenCalled();
        expect(service.delete).not.toHaveBeenCalled();
        expect(service.deleteAll).not.toHaveBeenCalled();
        expect(service.fetch).not.toHaveBeenCalled();
        expect(service.fetchAll).not.toHaveBeenCalled();
        expect(service.patch).not.toHaveBeenCalled();
      },
    );

    it.each`
      title
      ${''}
      ${' '}
    `(
      'should send bad request when title is blank',
      async ({ title }: { title: string }) => {
        await request(app.getHttpServer())
          .put('/todos/1')
          .send({ title, order: 1, completed: false })
          .expect(400)
          .expect((res: Response) => {
            expect(res.body).toEqual({
              error: 'Bad Request',
              message: ['title must not be blank'],
              statusCode: 400,
            });
          });

        expect(service.create).not.toHaveBeenCalled();
        expect(service.delete).not.toHaveBeenCalled();
        expect(service.deleteAll).not.toHaveBeenCalled();
        expect(service.fetch).not.toHaveBeenCalled();
        expect(service.fetchAll).not.toHaveBeenCalled();
        expect(service.patch).not.toHaveBeenCalled();
      },
    );

    it.each`
      order
      ${{}}
      ${[]}
    `(
      'should send bad request when invalid order',
      async ({ order }: { order: number }) => {
        await request(app.getHttpServer())
          .put('/todos/1')
          .send({ order, title: 'a title', completed: false })
          .expect(400)
          .expect((res: Response) => {
            expect(res.body).toEqual({
              error: 'Bad Request',
              message: [
                'order must be a number conforming to the specified constraints',
                'order must not be less than 1',
              ],
              statusCode: 400,
            });
          });

        expect(service.create).not.toHaveBeenCalled();
        expect(service.delete).not.toHaveBeenCalled();
        expect(service.deleteAll).not.toHaveBeenCalled();
        expect(service.fetch).not.toHaveBeenCalled();
        expect(service.fetchAll).not.toHaveBeenCalled();
        expect(service.patch).not.toHaveBeenCalled();
      },
    );

    it.each`
      order
      ${''}
      ${-1}
      ${0}
    `(
      'should send bad request when invalid order',
      async ({ order }: { order: number }) => {
        await request(app.getHttpServer())
          .put('/todos/1')
          .send({ order, title: 'a title', completed: false })
          .expect(400)
          .expect((res: Response) => {
            expect(res.body).toEqual({
              error: 'Bad Request',
              message: ['order must not be less than 1'],
              statusCode: 400,
            });
          });

        expect(service.create).not.toHaveBeenCalled();
        expect(service.delete).not.toHaveBeenCalled();
        expect(service.deleteAll).not.toHaveBeenCalled();
        expect(service.fetch).not.toHaveBeenCalled();
        expect(service.fetchAll).not.toHaveBeenCalled();
        expect(service.patch).not.toHaveBeenCalled();
      },
    );

    it.each`
      completed
      ${null}
      ${''}
      ${-1}
      ${0}
    `(
      'should send bad request when invalid order',
      async ({ completed }: { completed: boolean }) => {
        await request(app.getHttpServer())
          .put('/todos/1')
          .send({ completed, title: 'a title', order: 1 })
          .expect(400)
          .expect((res: Response) => {
            expect(res.body).toEqual({
              error: 'Bad Request',
              message: ['completed must be a boolean value'],
              statusCode: 400,
            });
          });

        expect(service.create).not.toHaveBeenCalled();
        expect(service.delete).not.toHaveBeenCalled();
        expect(service.deleteAll).not.toHaveBeenCalled();
        expect(service.fetch).not.toHaveBeenCalled();
        expect(service.fetchAll).not.toHaveBeenCalled();
        expect(service.patch).not.toHaveBeenCalled();
      },
    );

    it('should send conflict when conflicting order exception thrown', async () => {
      const error = new ConflictingOrderException('Conflicting order occurred');
      service.patch.mockRejectedValue(error);

      const body = {
        title: 'a title',
        order: 1,
        completed: false,
      };
      await request(app.getHttpServer())
        .put('/todos/1')
        .send(body)
        .expect(409)
        .expect((res: Response) => {
          expect(res.body).toEqual({
            message: 'Conflicting order occurred',
            statusCode: 409,
          });
        });

      expect(service.patch).toHaveBeenCalledTimes(1);
      expect(service.patch).toHaveBeenCalledWith('1', body);

      expect(service.create).not.toHaveBeenCalled();
      expect(service.delete).not.toHaveBeenCalled();
      expect(service.deleteAll).not.toHaveBeenCalled();
      expect(service.fetch).not.toHaveBeenCalled();
      expect(service.fetchAll).not.toHaveBeenCalled();
    });

    it('should send internal server error when service rejects', async () => {
      const error = new Error('An error occurred');
      service.patch.mockRejectedValue(error);

      const body = {
        title: 'a title',
        order: 1,
        completed: false,
      };
      await request(app.getHttpServer())
        .put('/todos/1')
        .send(body)
        .expect(500)
        .expect((res: Response) => {
          expect(res.body).toEqual({
            message: 'Internal server error',
            statusCode: 500,
          });
        });

      expect(service.patch).toHaveBeenCalledTimes(1);
      expect(service.patch).toHaveBeenCalledWith('1', body);

      expect(service.create).not.toHaveBeenCalled();
      expect(service.delete).not.toHaveBeenCalled();
      expect(service.deleteAll).not.toHaveBeenCalled();
      expect(service.fetch).not.toHaveBeenCalled();
      expect(service.fetchAll).not.toHaveBeenCalled();
    });
  });
});
