import { IService } from 'src/common/interfaces/service.interface';
import { TodoEntity } from '../entities/todo.entity';
import { TodoPatch } from '../types/todo.type';

export type ITodoService = IService<TodoEntity, TodoPatch>;

export const TODO_SERVICE_TOKEN = 'ITodoService';
