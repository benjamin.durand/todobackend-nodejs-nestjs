import { IRepository } from 'src/common/interfaces/repository.interface';
import { TodoEntity } from '../entities/todo.entity';

export type ITodoRepository = IRepository<TodoEntity>;

export const TODO_REPOSITORY_TOKEN = 'ITodoRepository';
