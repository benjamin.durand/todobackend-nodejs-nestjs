import { Test } from '@nestjs/testing';
import { TypeOrmModule } from '@nestjs/typeorm';
import {
  IUuidGenerator,
  UUID_GENERATOR_TOKEN,
} from 'src/common/helpers/uuid-generator.helper';
import { MockOf } from 'src/common/mocks/mock-of';
import { Connection, EntityManager, getConnection } from 'typeorm';
import { TodoEntity } from './entities/todo.entity';
import {
  ITodoRepository,
  TODO_REPOSITORY_TOKEN,
} from './interfaces/todo-repository.interface';
import { TypeOrmTodoRepository } from './typeorm-todo.repository';

describe('TypeOrmTodoRepository', () => {
  let repository: ITodoRepository;
  let uuidGenerator: MockOf<IUuidGenerator>;
  let connection: Connection;
  let entityManager: EntityManager;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [
        TypeOrmModule.forRoot({
          type: 'sqljs',
          autoLoadEntities: true,
          synchronize: true,
        }),
        TypeOrmModule.forFeature([TodoEntity]),
      ],
      providers: [
        {
          provide: UUID_GENERATOR_TOKEN,
          useValue: {
            generate: jest.fn(),
          },
        },
        {
          provide: TODO_REPOSITORY_TOKEN,
          useClass: TypeOrmTodoRepository,
        },
      ],
    }).compile();

    uuidGenerator = moduleRef.get<MockOf<IUuidGenerator>>(UUID_GENERATOR_TOKEN);
    repository = moduleRef.get<ITodoRepository>(TODO_REPOSITORY_TOKEN);

    connection = getConnection();
    entityManager = connection.createEntityManager();
  });

  afterEach(async () => {
    await connection.close();
  });

  it('should get last todo order', async () => {
    let count = await repository.getLastOrder();
    expect(count).toBe(0);

    await entityManager.insert<TodoEntity>(TodoEntity, {
      id: '1',
      title: 'a title',
      order: 1,
      completed: false,
    });

    count = await repository.getLastOrder();
    expect(count).toBe(1);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should create todo', async () => {
    const todo: TodoEntity = {
      id: undefined,
      title: 'a title',
      order: 1,
      completed: false,
    };
    uuidGenerator.generate.mockReturnValue(
      '68cbe45f-de73-4d4c-b643-ca0a3f5d4c7f',
    );

    const createdTodo = await repository.create(todo);
    expect(createdTodo).toEqual({
      id: '68cbe45f-de73-4d4c-b643-ca0a3f5d4c7f',
      title: 'a title',
      order: 1,
      completed: false,
    });

    const todos = await entityManager.find<TodoEntity>(TodoEntity);
    expect(todos).toEqual([createdTodo]);
    expect(uuidGenerator.generate).toHaveBeenCalledTimes(1);
    expect(uuidGenerator.generate).toHaveBeenCalledWith();
  });

  it('should delete todo', async () => {
    const todo: TodoEntity = {
      id: '1',
      title: 'a title',
      order: 1,
      completed: false,
    };
    await entityManager.insert<TodoEntity>(TodoEntity, todo);

    const todoDeleted = await repository.delete('1');
    expect(todoDeleted).toEqual({ ...todo, id: undefined });

    const todos = await entityManager.find<TodoEntity>(TodoEntity);
    expect(todos).toEqual([]);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should not delete when unknown todo id', async () => {
    const todoDeleted = await repository.delete('1');
    expect(todoDeleted).toBeUndefined();
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it.each`
    completed
    ${undefined}
    ${false}
  `('should delete all todos', async ({ completed }) => {
    await entityManager.insert<TodoEntity>(TodoEntity, [
      { id: '1', title: 'title 1', completed: false, order: 1 },
      { id: '2', title: 'title 2', completed: false, order: 2 },
    ]);

    await repository.deleteAll(completed);

    const todos = await entityManager.find<TodoEntity>(TodoEntity);
    expect(todos).toEqual([]);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should delete all completed todos', async () => {
    await entityManager.insert<TodoEntity>(TodoEntity, [
      { id: '1', title: 'title 1', completed: true, order: 1 },
      { id: '2', title: 'title 2', completed: false, order: 2 },
      { id: '3', title: 'title 3', completed: true, order: 3 },
    ]);

    await repository.deleteAll(true);

    const todos = await entityManager.find<TodoEntity>(TodoEntity);
    expect(todos).toEqual([
      {
        id: '2',
        title: 'title 2',
        completed: false,
        order: 2,
      },
    ]);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should fetch a todo by id', async () => {
    const todo1 = {
      id: '1',
      title: 'title 1',
      completed: true,
      order: 1,
    };
    const todo2 = {
      id: '2',
      title: 'title 2',
      completed: true,
      order: 2,
    };
    await entityManager.insert<TodoEntity>(TodoEntity, [todo1, todo2]);

    const fetchedTodo = await repository.fetch('2');
    expect(fetchedTodo).toEqual(todo2);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should return undefined when unknown todo id', async () => {
    const todo1 = {
      id: '1',
      title: 'title 1',
      completed: true,
      order: 1,
    };
    const todo2 = {
      id: '2',
      title: 'title 2',
      completed: true,
      order: 2,
    };
    await entityManager.insert<TodoEntity>(TodoEntity, [todo1, todo2]);

    const fetchedTodo = await repository.fetch('3');
    expect(fetchedTodo).toBeUndefined();
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should fetch todo by order', async () => {
    const todo1 = {
      id: '1',
      title: 'title 1',
      completed: true,
      order: 1,
    };
    const todo2 = {
      id: '2',
      title: 'title 2',
      completed: true,
      order: 2,
    };
    await entityManager.insert<TodoEntity>(TodoEntity, [todo1, todo2]);

    const fetchedTodo = await repository.fetchByOrder(2);
    expect(fetchedTodo).toEqual(todo2);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should return undefined when unknown todo order', async () => {
    const todo1 = {
      id: '1',
      title: 'title 1',
      completed: true,
      order: 1,
    };
    const todo2 = {
      id: '2',
      title: 'title 2',
      completed: true,
      order: 2,
    };
    await entityManager.insert<TodoEntity>(TodoEntity, [todo1, todo2]);

    const fetchedTodo = await repository.fetchByOrder(3);
    expect(fetchedTodo).toBeUndefined();
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should fetch all todos', async () => {
    const todo1 = {
      id: '1',
      title: 'title 1',
      completed: true,
      order: 1,
    };
    const todo2 = {
      id: '2',
      title: 'title 2',
      completed: true,
      order: 2,
    };
    await entityManager.insert<TodoEntity>(TodoEntity, [todo1, todo2]);

    const todos = await repository.fetchAll();
    expect(todos).toEqual([todo1, todo2]);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should fetch emtpy array when no todos', async () => {
    const todos = await repository.fetchAll();
    expect(todos).toEqual([]);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should patch a todo by id', async () => {
    const todo1 = {
      id: '1',
      title: 'title 1',
      completed: true,
      order: 1,
    };
    const todo2 = {
      id: '2',
      title: 'title 2',
      completed: true,
      order: 2,
    };
    await entityManager.insert<TodoEntity>(TodoEntity, [todo1, todo2]);

    const patchedTodo = await repository.patch('1', {
      completed: false,
      order: 3,
      title: 'title 1 updated',
    });
    expect(patchedTodo).toEqual({
      id: '1',
      title: 'title 1 updated',
      completed: false,
      order: 3,
    });

    const todos = await entityManager.find<TodoEntity>(TodoEntity);
    expect(todos).toEqual([patchedTodo, todo2]);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should return undefined when unknown todo id', async () => {
    const todo1 = {
      id: '1',
      title: 'title 1',
      completed: true,
      order: 1,
    };
    const todo2 = {
      id: '2',
      title: 'title 2',
      completed: true,
      order: 2,
    };
    await entityManager.insert<TodoEntity>(TodoEntity, [todo1, todo2]);

    const patchedTodo = await repository.patch('3', {
      completed: false,
      order: 3,
      title: 'title 1 updated',
    });
    expect(patchedTodo).toBeUndefined();

    const todos = await entityManager.find<TodoEntity>(TodoEntity);
    expect(todos).toEqual([todo1, todo2]);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should not patch a todo when no modification', async () => {
    const todo1 = {
      id: '1',
      title: 'title 1',
      completed: true,
      order: 1,
    };
    const todo2 = {
      id: '2',
      title: 'title 2',
      completed: true,
      order: 2,
    };
    await entityManager.insert<TodoEntity>(TodoEntity, [todo1, todo2]);

    const patchedTodo = await repository.patch('1', {});
    expect(patchedTodo).toEqual(todo1);

    const todos = await entityManager.find<TodoEntity>(TodoEntity);
    expect(todos).toEqual([patchedTodo, todo2]);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });
});
