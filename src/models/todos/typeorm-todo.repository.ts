import { Inject, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  IUuidGenerator,
  UUID_GENERATOR_TOKEN,
} from 'src/common/helpers/uuid-generator.helper';
import { Repository } from 'typeorm';
import { TodoEntity } from './entities/todo.entity';
import { ITodoRepository } from './interfaces/todo-repository.interface';
import { TodoPatch } from './types/todo.type';

@Injectable()
export class TypeOrmTodoRepository implements ITodoRepository {
  constructor(
    @InjectRepository(TodoEntity)
    private readonly repository: Repository<TodoEntity>,
    @Inject(UUID_GENERATOR_TOKEN)
    private readonly uuidGenerator: IUuidGenerator,
  ) {}

  async getLastOrder(): Promise<number> {
    const { lastOrder } = await this.repository
      .createQueryBuilder('todo')
      .select('MAX(todo.order)', 'lastOrder')
      .getRawOne();
    return lastOrder ?? 0;
  }

  async create(todo: TodoEntity): Promise<TodoEntity> {
    todo.id = this.uuidGenerator.generate();
    return this.repository.save(todo);
  }

  async delete(id: string): Promise<TodoEntity> {
    const todoToDelete = await this.repository.findOne(id);

    if (todoToDelete === undefined) {
      return undefined;
    }

    return this.repository.remove(todoToDelete);
  }

  async deleteAll(completed?: boolean): Promise<void> {
    let query = this.repository.createQueryBuilder().delete();

    if (completed) {
      query = query.where('completed = :completed', { completed });
    }

    await query.execute();
  }

  async fetch(id: string): Promise<TodoEntity> {
    return this.repository.findOne(id);
  }

  async fetchByOrder(order: number): Promise<TodoEntity | undefined> {
    return this.repository
      .createQueryBuilder('todo')
      .where('todo.order = :order', { order })
      .getOne();
  }

  async fetchAll(): Promise<TodoEntity[]> {
    return this.repository.find();
  }

  async patch(
    id: string,
    { title, completed, order }: TodoPatch,
  ): Promise<TodoEntity | undefined> {
    const todo = await this.fetch(id);

    if (!todo) {
      return undefined;
    }

    return this.repository.save({
      ...todo,
      title: title ?? todo.title,
      completed: completed ?? todo.completed,
      order: order ?? todo.order,
    });
  }
}
