import { MockOf } from 'src/common/mocks/mock-of';
import { TodoEntity } from './entities/todo.entity';
import { ITodoRepository } from './interfaces/todo-repository.interface';
import { ITodoService } from './interfaces/todo-service.interface';
import { TodoService } from './todo.service';
import { TodoPatch } from './types/todo.type';

describe('TodoService', () => {
  let todoRepository: MockOf<ITodoRepository>;
  let todoService: ITodoService;

  beforeEach(() => {
    todoRepository = {
      create: jest.fn(),
      delete: jest.fn(),
      deleteAll: jest.fn(),
      fetch: jest.fn(),
      fetchAll: jest.fn(),
      fetchByOrder: jest.fn(),
      getLastOrder: jest.fn(),
      patch: jest.fn(),
    };
    todoService = new TodoService(todoRepository);
  });

  describe('Creation', () => {
    it('should create a todo', async () => {
      const todo: TodoEntity = {
        id: '1',
        title: 'a title',
        order: 1,
        completed: false,
      };

      todoRepository.getLastOrder.mockReturnValue(Promise.resolve(0));
      todoRepository.create.mockReturnValue(Promise.resolve(todo));

      const title = 'a title';
      const result = await todoService.create(title);
      expect(result).toEqual(todo);

      expect(todoRepository.getLastOrder).toHaveBeenCalledTimes(1);
      expect(todoRepository.getLastOrder).toHaveBeenCalledWith();
      expect(todoRepository.create).toHaveBeenCalledTimes(1);
      expect(todoRepository.create).toHaveBeenCalledWith({
        title,
        order: 1,
        completed: false,
      });
      expect(
        todoRepository.getLastOrder.mock.invocationCallOrder[0],
      ).toBeLessThan(todoRepository.create.mock.invocationCallOrder[0]);

      expect(todoRepository.delete).not.toHaveBeenCalled();
      expect(todoRepository.fetch).not.toHaveBeenCalled();
      expect(todoRepository.fetchAll).not.toHaveBeenCalled();
      expect(todoRepository.fetchByOrder).not.toHaveBeenCalled();
      expect(todoRepository.deleteAll).not.toHaveBeenCalled();
      expect(todoRepository.patch).not.toHaveBeenCalled();
    });

    it('should reject promise when getLastOrder return an error', async () => {
      const error = new Error("Can't get last order");

      todoRepository.getLastOrder.mockReturnValue(Promise.reject(error));

      const title = 'a title';
      await expect(todoService.create(title)).rejects.toThrow(error);

      expect(todoRepository.getLastOrder).toHaveBeenCalledTimes(1);
      expect(todoRepository.getLastOrder).toHaveBeenCalledWith();

      expect(todoRepository.create).not.toHaveBeenCalled();
      expect(todoRepository.delete).not.toHaveBeenCalled();
      expect(todoRepository.fetch).not.toHaveBeenCalled();
      expect(todoRepository.fetchByOrder).not.toHaveBeenCalled();
      expect(todoRepository.fetchAll).not.toHaveBeenCalled();
      expect(todoRepository.deleteAll).not.toHaveBeenCalled();
      expect(todoRepository.patch).not.toHaveBeenCalled();
    });

    it('should reject promise when create return an error', async () => {
      const error = new Error("Can't create todo");

      todoRepository.getLastOrder.mockReturnValue(Promise.resolve(0));
      todoRepository.create.mockReturnValue(Promise.reject(error));

      const title = 'a title';
      await expect(todoService.create(title)).rejects.toThrow(error);

      expect(todoRepository.getLastOrder).toHaveBeenCalledTimes(1);
      expect(todoRepository.getLastOrder).toHaveBeenCalledWith();
      expect(todoRepository.create).toHaveBeenCalledTimes(1);
      expect(todoRepository.create).toHaveBeenCalledWith({
        title,
        order: 1,
        completed: false,
      });
      expect(
        todoRepository.getLastOrder.mock.invocationCallOrder[0],
      ).toBeLessThan(todoRepository.create.mock.invocationCallOrder[0]);

      expect(todoRepository.delete).not.toHaveBeenCalled();
      expect(todoRepository.fetch).not.toHaveBeenCalled();
      expect(todoRepository.fetchByOrder).not.toHaveBeenCalled();
      expect(todoRepository.fetchAll).not.toHaveBeenCalled();
      expect(todoRepository.deleteAll).not.toHaveBeenCalled();
      expect(todoRepository.patch).not.toHaveBeenCalled();
    });
  });

  describe('Deletion', () => {
    it('should delete a todo', async () => {
      todoRepository.delete.mockReturnValue(Promise.resolve(undefined));

      const todoId = '1';
      const result = await todoService.delete(todoId);
      expect(result).toBeUndefined();

      expect(todoRepository.delete).toHaveBeenCalledTimes(1);
      expect(todoRepository.delete).toHaveBeenCalledWith(todoId);

      expect(todoRepository.getLastOrder).not.toHaveBeenCalled();
      expect(todoRepository.create).not.toHaveBeenCalled();
      expect(todoRepository.fetch).not.toHaveBeenCalled();
      expect(todoRepository.fetchByOrder).not.toHaveBeenCalled();
      expect(todoRepository.fetchAll).not.toHaveBeenCalled();
      expect(todoRepository.deleteAll).not.toHaveBeenCalled();
      expect(todoRepository.patch).not.toHaveBeenCalled();
    });

    it('should reject promise when delete return an error', async () => {
      const error = new Error("Can't delete todo");

      todoRepository.delete.mockReturnValue(Promise.reject(error));

      const todoId = '1';
      await expect(todoService.delete(todoId)).rejects.toThrow(error);

      expect(todoRepository.delete).toHaveBeenCalledTimes(1);
      expect(todoRepository.delete).toHaveBeenCalledWith(todoId);

      expect(todoRepository.getLastOrder).not.toHaveBeenCalled();
      expect(todoRepository.create).not.toHaveBeenCalled();
      expect(todoRepository.fetch).not.toHaveBeenCalled();
      expect(todoRepository.fetchByOrder).not.toHaveBeenCalled();
      expect(todoRepository.fetchAll).not.toHaveBeenCalled();
      expect(todoRepository.deleteAll).not.toHaveBeenCalled();
      expect(todoRepository.patch).not.toHaveBeenCalled();
    });
  });

  describe('Complete deletion', () => {
    it.each`
      completed
      ${true}
      ${false}
    `(
      'should delete all todos',
      async ({ completed }: { completed: boolean }) => {
        todoRepository.deleteAll.mockReturnValue(Promise.resolve());

        const result = await todoService.deleteAll(completed);
        expect(result).toBeUndefined();

        expect(todoRepository.deleteAll).toHaveBeenCalledTimes(1);
        expect(todoRepository.deleteAll).toHaveBeenCalledWith(completed);

        expect(todoRepository.getLastOrder).not.toHaveBeenCalled();
        expect(todoRepository.create).not.toHaveBeenCalled();
        expect(todoRepository.fetch).not.toHaveBeenCalled();
        expect(todoRepository.fetchByOrder).not.toHaveBeenCalled();
        expect(todoRepository.fetchAll).not.toHaveBeenCalled();
        expect(todoRepository.delete).not.toHaveBeenCalled();
        expect(todoRepository.patch).not.toHaveBeenCalled();
      },
    );

    it.each`
      completed
      ${true}
      ${false}
    `(
      'should reject promise when deleteAll return an error',
      async ({ completed }: { completed: boolean }) => {
        const error = new Error("Can't deleteAll");

        todoRepository.deleteAll.mockReturnValue(Promise.reject(error));

        await expect(todoService.deleteAll(completed)).rejects.toThrow(error);

        expect(todoRepository.deleteAll).toHaveBeenCalledTimes(1);
        expect(todoRepository.deleteAll).toHaveBeenCalledWith(completed);

        expect(todoRepository.getLastOrder).not.toHaveBeenCalled();
        expect(todoRepository.create).not.toHaveBeenCalled();
        expect(todoRepository.fetch).not.toHaveBeenCalled();
        expect(todoRepository.fetchByOrder).not.toHaveBeenCalled();
        expect(todoRepository.fetchAll).not.toHaveBeenCalled();
        expect(todoRepository.delete).not.toHaveBeenCalled();
        expect(todoRepository.patch).not.toHaveBeenCalled();
      },
    );
  });

  describe('Fetching', () => {
    it('should fetch a todo', async () => {
      const todo: TodoEntity = {
        id: '1',
        title: 'a title',
        order: 1,
        completed: false,
      };

      todoRepository.fetch.mockReturnValue(Promise.resolve(todo));

      const todoId = '1';
      const result = await todoService.fetch(todoId);
      expect(result).toEqual(todo);

      expect(todoRepository.fetch).toHaveBeenCalledTimes(1);
      expect(todoRepository.fetch).toHaveBeenCalledWith(todoId);

      expect(todoRepository.getLastOrder).not.toHaveBeenCalled();
      expect(todoRepository.create).not.toHaveBeenCalled();
      expect(todoRepository.fetchByOrder).not.toHaveBeenCalled();
      expect(todoRepository.fetchAll).not.toHaveBeenCalled();
      expect(todoRepository.delete).not.toHaveBeenCalled();
      expect(todoRepository.deleteAll).not.toHaveBeenCalled();
      expect(todoRepository.patch).not.toHaveBeenCalled();
    });

    it('should reject promise when fetch return an error', async () => {
      const error = new Error("Can't fetch todo");

      todoRepository.fetch.mockReturnValue(Promise.reject(error));

      const todoId = '1';
      await expect(todoService.fetch(todoId)).rejects.toThrow(error);

      expect(todoRepository.fetch).toHaveBeenCalledTimes(1);
      expect(todoRepository.fetch).toHaveBeenCalledWith(todoId);

      expect(todoRepository.getLastOrder).not.toHaveBeenCalled();
      expect(todoRepository.create).not.toHaveBeenCalled();
      expect(todoRepository.fetchByOrder).not.toHaveBeenCalled();
      expect(todoRepository.fetchAll).not.toHaveBeenCalled();
      expect(todoRepository.delete).not.toHaveBeenCalled();
      expect(todoRepository.deleteAll).not.toHaveBeenCalled();
      expect(todoRepository.patch).not.toHaveBeenCalled();
    });
  });

  describe('Fetching all', () => {
    it('should fetch todos ordered', async () => {
      const todos: TodoEntity[] = [
        {
          id: '1',
          title: 'a title',
          order: 1,
          completed: false,
        },
        {
          id: '2',
          title: 'another title',
          order: 2,
          completed: false,
        },
        {
          id: '3',
          title: 'title',
          order: 3,
          completed: false,
        },
      ];

      todoRepository.fetchAll.mockReturnValue(Promise.resolve(todos));

      const result = await todoService.fetchAll();
      expect(result).toEqual(todos);
      expect(result.map((todo) => todo.id)).toEqual(['1', '2', '3']);

      expect(todoRepository.fetchAll).toHaveBeenCalledTimes(1);
      expect(todoRepository.fetchAll).toHaveBeenCalledWith();

      expect(todoRepository.getLastOrder).not.toHaveBeenCalled();
      expect(todoRepository.create).not.toHaveBeenCalled();
      expect(todoRepository.fetch).not.toHaveBeenCalled();
      expect(todoRepository.fetchByOrder).not.toHaveBeenCalled();
      expect(todoRepository.delete).not.toHaveBeenCalled();
      expect(todoRepository.deleteAll).not.toHaveBeenCalled();
      expect(todoRepository.patch).not.toHaveBeenCalled();
    });

    it('should reject promise when fetchAll return an error', async () => {
      const error = new Error("Can't fetch todos");

      todoRepository.fetchAll.mockReturnValue(Promise.reject(error));

      await expect(todoService.fetchAll()).rejects.toThrow(error);

      expect(todoRepository.fetchAll).toHaveBeenCalledTimes(1);
      expect(todoRepository.fetchAll).toHaveBeenCalledWith();

      expect(todoRepository.getLastOrder).not.toHaveBeenCalled();
      expect(todoRepository.create).not.toHaveBeenCalled();
      expect(todoRepository.fetch).not.toHaveBeenCalled();
      expect(todoRepository.fetchByOrder).not.toHaveBeenCalled();
      expect(todoRepository.delete).not.toHaveBeenCalled();
      expect(todoRepository.deleteAll).not.toHaveBeenCalled();
      expect(todoRepository.patch).not.toHaveBeenCalled();
    });
  });

  describe('Modification', () => {
    it('should patch a todo with no conflicting order', async () => {
      const todo: TodoEntity = {
        id: '1',
        title: 'a title',
        order: 1,
        completed: false,
      };

      todoRepository.fetchByOrder.mockReturnValue(Promise.resolve(undefined));
      todoRepository.patch.mockReturnValue(Promise.resolve(todo));

      const todoId = '1';
      const todoPatch: TodoPatch = {
        completed: true,
        order: 2,
        title: 'summary',
      };
      const result = await todoService.patch(todoId, todoPatch);
      expect(result).toEqual(todo);

      expect(todoRepository.fetchByOrder).toHaveBeenCalledTimes(1);
      expect(todoRepository.fetchByOrder).toHaveBeenCalledWith(2);
      expect(todoRepository.patch).toHaveBeenCalledTimes(1);
      expect(todoRepository.patch).toHaveBeenCalledWith(todoId, todoPatch);
      expect(
        todoRepository.fetchByOrder.mock.invocationCallOrder[0],
      ).toBeLessThan(todoRepository.patch.mock.invocationCallOrder[0]);

      expect(todoRepository.getLastOrder).not.toHaveBeenCalled();
      expect(todoRepository.create).not.toHaveBeenCalled();
      expect(todoRepository.fetch).not.toHaveBeenCalled();
      expect(todoRepository.fetchAll).not.toHaveBeenCalled();
      expect(todoRepository.delete).not.toHaveBeenCalled();
      expect(todoRepository.deleteAll).not.toHaveBeenCalled();
    });

    it('should patch a todo with same order', async () => {
      const todo: TodoEntity = {
        id: '1',
        title: 'a title',
        order: 1,
        completed: false,
      };

      todoRepository.fetchByOrder.mockReturnValue(Promise.resolve(todo));
      todoRepository.patch.mockReturnValue(Promise.resolve(todo));

      const todoId = '1';
      const todoPatch: TodoPatch = {
        completed: true,
        order: 2,
        title: 'summary',
      };
      const result = await todoService.patch(todoId, todoPatch);
      expect(result).toEqual(todo);

      expect(todoRepository.fetchByOrder).toHaveBeenCalledTimes(1);
      expect(todoRepository.fetchByOrder).toHaveBeenCalledWith(2);
      expect(todoRepository.patch).toHaveBeenCalledTimes(1);
      expect(todoRepository.patch).toHaveBeenCalledWith(todoId, todoPatch);
      expect(
        todoRepository.fetchByOrder.mock.invocationCallOrder[0],
      ).toBeLessThan(todoRepository.patch.mock.invocationCallOrder[0]);

      expect(todoRepository.getLastOrder).not.toHaveBeenCalled();
      expect(todoRepository.create).not.toHaveBeenCalled();
      expect(todoRepository.fetch).not.toHaveBeenCalled();
      expect(todoRepository.fetchAll).not.toHaveBeenCalled();
      expect(todoRepository.delete).not.toHaveBeenCalled();
      expect(todoRepository.deleteAll).not.toHaveBeenCalled();
    });

    it('should throw exception when conflicting order', async () => {
      const todo: TodoEntity = {
        id: '1',
        title: 'a title',
        order: 1,
        completed: false,
      };

      todoRepository.fetchByOrder.mockReturnValue(
        Promise.resolve({ ...todo, id: '2' }),
      );

      const todoId = '1';
      const todoPatch: TodoPatch = {
        completed: true,
        order: 2,
        title: 'summary',
      };
      await expect(todoService.patch(todoId, todoPatch)).rejects.toThrowError(
        'Conflicting order occurred, order:2 already exists',
      );

      expect(todoRepository.fetchByOrder).toHaveBeenCalledTimes(1);
      expect(todoRepository.fetchByOrder).toHaveBeenCalledWith(2);

      expect(todoRepository.getLastOrder).not.toHaveBeenCalled();
      expect(todoRepository.create).not.toHaveBeenCalled();
      expect(todoRepository.fetch).not.toHaveBeenCalled();
      expect(todoRepository.fetchAll).not.toHaveBeenCalled();
      expect(todoRepository.delete).not.toHaveBeenCalled();
      expect(todoRepository.deleteAll).not.toHaveBeenCalled();
      expect(todoRepository.patch).not.toHaveBeenCalled();
    });

    it('should patch a todo without fetching by order', async () => {
      const todo: TodoEntity = {
        id: '1',
        title: 'a title',
        order: 1,
        completed: false,
      };

      todoRepository.patch.mockReturnValue(Promise.resolve(todo));

      const todoId = '1';
      const todoPatch: TodoPatch = {
        completed: true,
        title: 'summary',
      };
      const result = await todoService.patch(todoId, todoPatch);
      expect(result).toEqual(todo);

      expect(todoRepository.patch).toHaveBeenCalledTimes(1);
      expect(todoRepository.patch).toHaveBeenCalledWith(todoId, todoPatch);

      expect(todoRepository.getLastOrder).not.toHaveBeenCalled();
      expect(todoRepository.create).not.toHaveBeenCalled();
      expect(todoRepository.fetch).not.toHaveBeenCalled();
      expect(todoRepository.fetchByOrder).not.toHaveBeenCalled();
      expect(todoRepository.fetchAll).not.toHaveBeenCalled();
      expect(todoRepository.delete).not.toHaveBeenCalled();
      expect(todoRepository.deleteAll).not.toHaveBeenCalled();
    });

    it('should return an error when fetchByOrder rejects', async () => {
      const error = new Error("Can't fetch todo by order");

      todoRepository.fetchByOrder.mockReturnValue(Promise.reject(error));

      const todoId = '1';
      const todoPatch: TodoPatch = {
        completed: true,
        order: 2,
        title: 'summary',
      };
      await expect(todoService.patch(todoId, todoPatch)).rejects.toThrow(error);

      expect(todoRepository.fetchByOrder).toHaveBeenCalledTimes(1);
      expect(todoRepository.fetchByOrder).toHaveBeenCalledWith(2);

      expect(todoRepository.getLastOrder).not.toHaveBeenCalled();
      expect(todoRepository.create).not.toHaveBeenCalled();
      expect(todoRepository.fetch).not.toHaveBeenCalled();
      expect(todoRepository.fetchAll).not.toHaveBeenCalled();
      expect(todoRepository.delete).not.toHaveBeenCalled();
      expect(todoRepository.deleteAll).not.toHaveBeenCalled();
      expect(todoRepository.patch).not.toHaveBeenCalled();
    });

    it('should return an error when patch rejects', async () => {
      const error = new Error("Can't patch todo");

      todoRepository.fetchByOrder.mockReturnValue(Promise.resolve(undefined));
      todoRepository.patch.mockReturnValue(Promise.reject(error));

      const todoId = '1';
      const todoPatch: TodoPatch = {
        completed: true,
        order: 2,
        title: 'summary',
      };
      await expect(todoService.patch(todoId, todoPatch)).rejects.toThrow(error);

      expect(todoRepository.fetchByOrder).toHaveBeenCalledTimes(1);
      expect(todoRepository.fetchByOrder).toHaveBeenCalledWith(2);
      expect(todoRepository.patch).toHaveBeenCalledTimes(1);
      expect(todoRepository.patch).toHaveBeenCalledWith(todoId, todoPatch);
      expect(
        todoRepository.fetchByOrder.mock.invocationCallOrder[0],
      ).toBeLessThan(todoRepository.patch.mock.invocationCallOrder[0]);

      expect(todoRepository.getLastOrder).not.toHaveBeenCalled();
      expect(todoRepository.create).not.toHaveBeenCalled();
      expect(todoRepository.fetch).not.toHaveBeenCalled();
      expect(todoRepository.fetchAll).not.toHaveBeenCalled();
      expect(todoRepository.delete).not.toHaveBeenCalled();
      expect(todoRepository.deleteAll).not.toHaveBeenCalled();
    });
  });
});
