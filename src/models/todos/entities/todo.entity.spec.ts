import { TodoEntity } from './todo.entity';

describe('TodoEntity', () => {
  it('should create a todo', () => {
    const todo = new TodoEntity('a title', 2, true);
    expect(todo).toEqual({
      id: undefined,
      title: 'a title',
      order: 2,
      completed: true,
    });
  });
});
