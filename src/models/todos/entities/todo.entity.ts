import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity()
export class TodoEntity {
  @PrimaryColumn()
  id: string;

  @Column()
  title: string;

  @Column()
  order: number;

  @Column()
  completed: boolean;

  constructor(title: string, order: number, completed: boolean) {
    this.title = title;
    this.order = order;
    this.completed = completed;
  }
}
