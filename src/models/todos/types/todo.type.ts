import { TodoEntity } from '../entities/todo.entity';

export type TodoPatch = Partial<
  Pick<TodoEntity, 'title' | 'order' | 'completed'>
>;
