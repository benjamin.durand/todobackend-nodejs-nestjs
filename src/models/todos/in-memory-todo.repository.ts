import { Inject, Injectable } from '@nestjs/common';
import {
  IUuidGenerator,
  UUID_GENERATOR_TOKEN,
} from 'src/common/helpers/uuid-generator.helper';
import { TodoEntity } from './entities/todo.entity';
import { ITodoRepository } from './interfaces/todo-repository.interface';
import { TodoPatch } from './types/todo.type';

@Injectable()
export class InMemoryTodoRepository implements ITodoRepository {
  private todos: TodoEntity[] = [];

  constructor(
    @Inject(UUID_GENERATOR_TOKEN)
    private readonly uuidGenerator: IUuidGenerator,
  ) {}

  async getLastOrder(): Promise<number> {
    const order =
      this.todos.length > 0 ? this.todos[this.todos.length - 1].order : 0;
    return order;
  }

  async create(todoToSave: TodoEntity): Promise<TodoEntity> {
    const todo = { ...todoToSave, id: this.uuidGenerator.generate() };
    this.todos = [...this.todos, todo];
    return todo;
  }

  async delete(id: string): Promise<TodoEntity | undefined> {
    const todoToDelete = this.todos.find((t) => t.id === id);

    if (todoToDelete === undefined) {
      return undefined;
    }

    this.todos = this.todos.filter((todo) => todo.id !== todoToDelete.id);
    return todoToDelete;
  }

  async deleteAll(completed = false): Promise<void> {
    if (completed === false) {
      this.todos = [];
    } else {
      this.todos = this.todos.filter(
        (todo: TodoEntity) => todo.completed !== completed,
      );
    }
  }

  async fetch(id: string): Promise<TodoEntity | undefined> {
    const todo = this.todos.find((todo: TodoEntity) => todo.id === id);
    return todo;
  }

  async fetchByOrder(order: number): Promise<TodoEntity | undefined> {
    const todo = this.todos.find((todo: TodoEntity) => todo.order === order);
    return todo;
  }

  async fetchAll(): Promise<TodoEntity[]> {
    return this.todos;
  }

  async patch(
    id: string,
    { completed, order, title }: TodoPatch,
  ): Promise<TodoEntity | undefined> {
    const index = this.todos.findIndex((todo: TodoEntity) => todo.id === id);

    if (index === -1) {
      return undefined;
    }

    this.todos = this.todos.map((todo, i) =>
      i === index
        ? {
            ...todo,
            title: title ?? todo.title,
            completed: completed ?? todo.completed,
            order: order ?? todo.order,
          }
        : todo,
    );
    return this.todos[index];
  }
}
