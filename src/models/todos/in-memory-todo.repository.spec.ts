import { IUuidGenerator } from 'src/common/helpers/uuid-generator.helper';
import { MockOf } from 'src/common/mocks/mock-of';
import { TodoEntity } from './entities/todo.entity';
import { InMemoryTodoRepository } from './in-memory-todo.repository';
import { ITodoRepository } from './interfaces/todo-repository.interface';

describe('InMemoryTodoRepository', () => {
  let repository: ITodoRepository;
  let uuidGenerator: MockOf<IUuidGenerator>;

  beforeEach(() => {
    uuidGenerator = {
      generate: jest.fn(),
    };
    repository = new InMemoryTodoRepository(uuidGenerator);
  });

  it('should get last todo order', async () => {
    let count = await repository.getLastOrder();
    expect(count).toBe(0);

    (repository as any).todos = [{ order: 1 }];

    count = await repository.getLastOrder();
    expect(count).toBe(1);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should create todo', async () => {
    const todo: TodoEntity = {
      id: undefined,
      title: 'title',
      order: 1,
      completed: false,
    };
    uuidGenerator.generate.mockReturnValue(
      '1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed',
    );
    const createdTodo = await repository.create(todo);

    expect(createdTodo).toEqual({
      id: '1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed',
      title: 'title',
      order: 1,
      completed: false,
    });
    expect((repository as any).todos).toEqual([createdTodo]);
    expect(uuidGenerator.generate).toHaveBeenCalledTimes(1);
    expect(uuidGenerator.generate).toHaveBeenCalledWith();
  });

  it('should delete todo', async () => {
    const todo: TodoEntity = {
      id: '1',
      title: 'title',
      order: 1,
      completed: false,
    };

    (repository as any).todos = [todo];

    const todoDeleted = await repository.delete('1');

    expect(todoDeleted).toBe(todo);
    expect((repository as any).todos).toEqual([]);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should not delete when unknown todo id', async () => {
    const todoDeleted = await repository.delete('1');
    expect(todoDeleted).toBeUndefined();
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it.each`
    completed
    ${undefined}
    ${false}
  `('should delete all todos', async ({ completed }) => {
    ((repository as any).todos as TodoEntity[]) = [
      { id: '1', title: 'title 1', completed: false, order: 1 },
      { id: '2', title: 'title 2', completed: false, order: 2 },
    ];

    await repository.deleteAll(completed);

    expect((repository as any).todos).toEqual([]);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should delete all completed todos', async () => {
    ((repository as any).todos as TodoEntity[]) = [
      { id: '1', title: 'title 1', completed: true, order: 1 },
      { id: '2', title: 'title 2', completed: false, order: 2 },
      { id: '3', title: 'title 3', completed: true, order: 3 },
    ];

    await repository.deleteAll(true);

    expect((repository as any).todos).toEqual([
      {
        id: '2',
        title: 'title 2',
        completed: false,
        order: 2,
      },
    ]);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should fetch a todo by id', async () => {
    const todo1 = {
      id: '1',
      title: 'title 1',
      completed: true,
      order: 1,
      url: 'http://localhost/1',
    };
    const todo2 = {
      id: '2',
      title: 'title 2',
      completed: true,
      order: 2,
      url: 'http://localhost/2',
    };
    ((repository as any).todos as TodoEntity[]) = [todo1, todo2];

    const fetchedTodo = await repository.fetch('2');
    expect(fetchedTodo).toEqual(todo2);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should return undefined when unknown todo id', async () => {
    const todo1 = {
      id: '1',
      title: 'title 1',
      completed: true,
      order: 1,
      url: 'http://localhost/1',
    };
    const todo2 = {
      id: '2',
      title: 'title 2',
      completed: true,
      order: 2,
      url: 'http://localhost/2',
    };
    ((repository as any).todos as TodoEntity[]) = [todo1, todo2];

    const fetchedTodo = await repository.fetch('3');
    expect(fetchedTodo).toBeUndefined();
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should fetch todo by order', async () => {
    const todo1 = {
      id: '1',
      title: 'title 1',
      completed: true,
      order: 1,
      url: 'http://localhost/1',
    };
    const todo2 = {
      id: '2',
      title: 'title 2',
      completed: true,
      order: 2,
      url: 'http://localhost/2',
    };
    ((repository as any).todos as TodoEntity[]) = [todo1, todo2];

    const fetchedTodo = await repository.fetchByOrder(2);
    expect(fetchedTodo).toEqual(todo2);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should return undefined when unknown todo order', async () => {
    const todo1 = {
      id: '1',
      title: 'title 1',
      completed: true,
      order: 1,
      url: 'http://localhost/1',
    };
    const todo2 = {
      id: '2',
      title: 'title 2',
      completed: true,
      order: 2,
      url: 'http://localhost/2',
    };
    ((repository as any).todos as TodoEntity[]) = [todo1, todo2];

    const fetchedTodo = await repository.fetchByOrder(3);
    expect(fetchedTodo).toBeUndefined();
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should fetch all todos', async () => {
    const todo1 = {
      id: '1',
      title: 'title 1',
      completed: true,
      order: 1,
      url: 'http://localhost/1',
    };
    const todo2 = {
      id: '2',
      title: 'title 2',
      completed: true,
      order: 2,
      url: 'http://localhost/2',
    };
    ((repository as any).todos as TodoEntity[]) = [todo1, todo2];

    const todos = await repository.fetchAll();
    expect((repository as any).todos).toBe(todos);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should fetch emtpy array when no todos', async () => {
    ((repository as any).todos as TodoEntity[]) = [];

    const todos = await repository.fetchAll();
    expect(todos).toEqual([]);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should patch a todo by id', async () => {
    const todo1 = {
      id: '1',
      title: 'title 1',
      completed: true,
      order: 1,
      url: 'http://localhost/1',
    };
    const todo2 = {
      id: '2',
      title: 'title 2',
      completed: true,
      order: 2,
      url: 'http://localhost/2',
    };
    ((repository as any).todos as TodoEntity[]) = [todo1, todo2];

    const patchedTodo = await repository.patch('1', {
      completed: false,
      order: 3,
      title: 'title 1 updated',
    });

    expect(patchedTodo).toEqual({
      id: '1',
      title: 'title 1 updated',
      completed: false,
      order: 3,
      url: 'http://localhost/1',
    });
    expect((repository as any).todos).toEqual([patchedTodo, todo2]);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should return undefined when unknown todo id', async () => {
    const todo1 = {
      id: '1',
      title: 'title 1',
      completed: true,
      order: 1,
      url: 'http://localhost/1',
    };
    const todo2 = {
      id: '2',
      title: 'title 2',
      completed: true,
      order: 2,
      url: 'http://localhost/2',
    };
    ((repository as any).todos as TodoEntity[]) = [todo1, todo2];

    const patchedTodo = await repository.patch('3', {
      completed: false,
      order: 3,
      title: 'title 1 updated',
    });

    expect(patchedTodo).toBeUndefined();
    expect((repository as any).todos).toEqual([todo1, todo2]);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });

  it('should not patch a todo when no modification', async () => {
    const todo1 = {
      id: '1',
      title: 'title 1',
      completed: true,
      order: 1,
      url: 'http://localhost/1',
    };
    const todo2 = {
      id: '2',
      title: 'title 2',
      completed: true,
      order: 2,
      url: 'http://localhost/2',
    };
    ((repository as any).todos as TodoEntity[]) = [todo1, todo2];

    const patchedTodo = await repository.patch('1', {});

    expect(patchedTodo).toEqual(todo1);
    expect((repository as any).todos).toEqual([patchedTodo, todo2]);
    expect(uuidGenerator.generate).not.toHaveBeenCalled();
  });
});
