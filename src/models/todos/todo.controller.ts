import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Inject,
  NotFoundException,
  Param,
  Patch,
  Post,
  Put,
  Query,
  Req,
  UseFilters,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { TODO_ENPOINT } from 'src/common/constants/endpoints';
import { UrlHelper } from 'src/common/helpers/url.helper';
import { RequestModel } from 'src/common/middlewares/uri.middleware';
import { ConflictingOrderExceptionFilter } from '../../common/exceptions/conflicting-order-exception.filter';
import {
  TodoCreateDto,
  TodoDeleteQueryDto,
  TodoPatchDto,
  TodoResponseDto,
  TodoUpdateDto,
} from './dtos/todo.dto';
import {
  ITodoService,
  TODO_SERVICE_TOKEN,
} from './interfaces/todo-service.interface';

@Controller(TODO_ENPOINT)
@UsePipes(new ValidationPipe({ transform: true }))
export class TodoController {
  constructor(
    @Inject(TODO_SERVICE_TOKEN) private readonly todoService: ITodoService,
    private readonly urlUtil: UrlHelper,
  ) {}

  @Post()
  async addTodo(
    @Body() todoCreateDto: TodoCreateDto,
    @Req() req: RequestModel,
  ): Promise<TodoResponseDto> {
    const todo = await this.todoService.create(todoCreateDto.title);
    return {
      ...todo,
      url: this.urlUtil.generate(req.uri, TODO_ENPOINT, todo.id),
    };
  }

  @Delete(':id')
  @HttpCode(HttpStatus.NO_CONTENT)
  async deleteTodo(@Param('id') id: string): Promise<void> {
    const todo = await this.todoService.delete(id);
    if (!todo) {
      throw new NotFoundException();
    }
  }

  @Delete()
  @HttpCode(HttpStatus.NO_CONTENT)
  async deleteTodos(
    @Query() { completed = false }: TodoDeleteQueryDto,
  ): Promise<void> {
    return this.todoService.deleteAll(completed);
  }

  @Get(':id')
  async getTodo(
    @Param('id') id: string,
    @Req() req: RequestModel,
  ): Promise<TodoResponseDto> {
    const todo = await this.todoService.fetch(id);
    if (!todo) {
      throw new NotFoundException();
    }
    return {
      ...todo,
      url: this.urlUtil.generate(req.uri, TODO_ENPOINT, todo.id),
    };
  }

  @Get()
  async getTodos(@Req() req: RequestModel): Promise<TodoResponseDto[]> {
    const todos = await this.todoService.fetchAll();
    return todos.map((todo) => ({
      ...todo,
      url: this.urlUtil.generate(req.uri, TODO_ENPOINT, todo.id),
    }));
  }

  @Patch(':id')
  @UseFilters(ConflictingOrderExceptionFilter)
  async patchTodo(
    @Param('id') id: string,
    @Body() todoPatchDto: TodoPatchDto,
    @Req() req: RequestModel,
  ): Promise<TodoResponseDto> {
    const todo = await this.todoService.patch(id, todoPatchDto);
    if (!todo) {
      throw new NotFoundException();
    }
    return {
      ...todo,
      url: this.urlUtil.generate(req.uri, TODO_ENPOINT, todo.id),
    };
  }

  @Put(':id')
  @UseFilters(ConflictingOrderExceptionFilter)
  async updateTodo(
    @Param('id') id: string,
    @Body() todoUpdateDto: TodoUpdateDto,
    @Req() req: RequestModel,
  ): Promise<TodoResponseDto> {
    const todo = await this.todoService.patch(id, todoUpdateDto);
    if (!todo) {
      throw new NotFoundException();
    }
    return {
      ...todo,
      url: this.urlUtil.generate(req.uri, TODO_ENPOINT, todo.id),
    };
  }
}
