import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppConfigService } from 'src/config/app/config.service';
import { DatabaseConfigService } from 'src/config/database/config.service';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      useFactory: async (
        appConfigService: AppConfigService,
        databaseConfigService: DatabaseConfigService,
      ) => ({
        type: databaseConfigService.type as any,
        host: databaseConfigService.host,
        port: databaseConfigService.port,
        username: databaseConfigService.username,
        password: databaseConfigService.password,
        database: databaseConfigService.name,
        autoLoadEntities: true,
        synchronize: !appConfigService.isDev,
      }),
      inject: [AppConfigService, DatabaseConfigService],
    }),
  ],
})
export class DatabaseProviderModule {}
