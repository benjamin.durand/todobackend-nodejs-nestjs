import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { Response } from 'express';
import { ConflictingOrderException } from './conflicting-order.exception';

@Catch(ConflictingOrderException)
export class ConflictingOrderExceptionFilter
  implements ExceptionFilter<ConflictingOrderException>
{
  catch(exception: ConflictingOrderException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const statusCode = 409;
    const message = exception.message;

    response.status(statusCode).json({
      message,
      statusCode,
    });
  }
}
