import { Injectable, NestMiddleware } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import * as URI from 'urijs';

export interface RequestModel extends Request {
  uri: URI;
}

@Injectable()
export class UriMiddleware implements NestMiddleware {
  use(req: RequestModel, _: Response, next: NextFunction) {
    req.uri = new URI({
      protocol: req.protocol,
      hostname: req.headers.host,
      path: req.url,
    });
    next();
  }
}
