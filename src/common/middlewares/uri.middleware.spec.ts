const uri = {
  protocol: 'http',
  hostname: 'localhost',
  path: '/todos',
};
const mockFn = jest.fn().mockImplementation(() => {
  return uri;
});
jest.mock('urijs', () => {
  return mockFn;
});

import { UriMiddleware } from './uri.middleware';

describe('UriMiddleware', () => {
  let uriMiddleware: UriMiddleware;

  beforeEach(() => {
    uriMiddleware = new UriMiddleware();
  });

  it('should add uri property to request object', () => {
    const req: any = {
      protocol: 'http',
      headers: {
        host: 'localhost',
      },
      url: '/todos',
    };
    const res: any = jest.fn();
    const next = jest.fn();

    uriMiddleware.use(req, res, next);
    expect(req.uri).toBe(uri);
    expect(next).toHaveBeenCalledTimes(1);
    expect(next).toHaveBeenCalledWith();
    expect(mockFn).toHaveBeenCalledTimes(1);
    expect(mockFn).toHaveBeenCalledWith({
      protocol: 'http',
      hostname: 'localhost',
      path: '/todos',
    });
  });
});
