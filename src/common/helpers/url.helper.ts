import { Injectable } from '@nestjs/common';

@Injectable()
export class UrlHelper {
  generate(uri: URI, endpoint: string, id: string): string {
    return uri.segment([endpoint, id]).toString();
  }
}
