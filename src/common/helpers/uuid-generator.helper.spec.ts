const UUID_VALUE = '68cbe45f-de73-4d4c-b643-ca0a3f5d4c7f';

const mockFn = jest.fn().mockReturnValue(UUID_VALUE);
jest.mock('uuid', () => ({
  v4: mockFn,
}));

import { IUuidGenerator, UuidGenerator } from './uuid-generator.helper';

describe('UuidGenerator', () => {
  let uuidGenerator: IUuidGenerator;

  beforeEach(() => {
    uuidGenerator = new UuidGenerator();
  });

  it('should generate an uuid through uuid module', () => {
    expect(uuidGenerator.generate()).toBe(UUID_VALUE);
    expect(mockFn).toHaveBeenCalledTimes(1);
    expect(mockFn).toHaveBeenCalledWith();
  });
});
