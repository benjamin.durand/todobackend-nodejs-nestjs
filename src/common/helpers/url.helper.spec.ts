import * as URI from 'urijs';
import { UrlHelper } from './url.helper';

describe('UrlHelper', () => {
  let urlUtil: UrlHelper;

  beforeEach(() => {
    urlUtil = new UrlHelper();
  });

  it('should generate an url from an uri', () => {
    const uri = new URI({
      protocol: 'http',
      path: '/todos',
      hostname: 'localhost',
    });
    const url = urlUtil.generate(uri, 'todos', '1');
    expect(url).toBe('http://localhost/todos/1');
  });
});
