import { Injectable } from '@nestjs/common';
import { v4 as uuidv4 } from 'uuid';

export const UUID_GENERATOR_TOKEN = 'IUuidGenerator';

export interface IUuidGenerator {
  generate: () => string;
}

@Injectable()
export class UuidGenerator implements IUuidGenerator {
  generate(): string {
    return uuidv4();
  }
}
