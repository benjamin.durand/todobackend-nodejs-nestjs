export type MockOf<T> = {
  [P in keyof T]: jest.Mock<
    T[P] extends (...args: any) => any ? ReturnType<T[P]> : any,
    T[P] extends (...args: any) => any ? Parameters<T[P]> : any
  >;
};
