export interface IRepository<T> {
  getLastOrder: () => Promise<number>;

  create: (entity: T) => Promise<T>;

  delete: (id: string) => Promise<T | undefined>;

  deleteAll: (completed?: boolean) => Promise<void>;

  fetch: (id: string) => Promise<T | undefined>;

  fetchByOrder: (order: number) => Promise<T | undefined>;

  fetchAll: () => Promise<T[]>;

  patch: (id: string, entityPatch: Partial<T>) => Promise<T | undefined>;
}
