export interface IService<Entity, Patch> {
  create(title: string): Promise<Entity>;

  delete(id: string): Promise<Entity | undefined>;

  deleteAll(completed: boolean): Promise<void>;

  fetch(id: string): Promise<Entity | undefined>;

  fetchAll(): Promise<Entity[]>;

  patch(id: string, patch: Patch): Promise<Entity | undefined>;
}
