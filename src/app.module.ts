import { Module } from '@nestjs/common';
import { AppConfigModule } from './config/app/config.module';
import { DatabaseConfigModule } from './config/database/config.module';
import { TodoModule } from './models/todos/todo.module';
import { DatabaseProviderModule } from './providers/database/provider.module';

@Module({
  imports: [
    AppConfigModule,
    DatabaseConfigModule,
    DatabaseProviderModule,
    TodoModule,
  ],
})
export class AppModule {}
