import { registerAs } from '@nestjs/config';

export default registerAs('app', () => ({
  isDev: process.env.ENV === 'DEV',
  port: process.env.PORT,
}));
