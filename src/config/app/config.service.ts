import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AppConfigService {
  constructor(private configService: ConfigService) {}

  get isDev(): boolean {
    return this.configService.get<boolean>('app.isDev');
  }

  get port(): number {
    return this.configService.get<number>('app.port');
  }
}
