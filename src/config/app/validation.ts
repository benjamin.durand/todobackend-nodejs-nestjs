import { plainToClass } from 'class-transformer';
import { IsEnum, IsNumber, validateSync } from 'class-validator';

enum Environment {
  DEV = 'DEV',
  INT = 'INT',
  QA = 'QA',
  VABF = 'VABF',
  MCO = 'MCO',
  PROD = 'PROD',
}

class AppEnvironmentVariables {
  @IsEnum(Environment)
  ENV: Environment;

  // eslint-disable-next-line @typescript-eslint/no-inferrable-types
  @IsNumber()
  PORT: number = 3000;
}

export default (config: Record<string, unknown>): AppEnvironmentVariables => {
  const validatedConfig = plainToClass(AppEnvironmentVariables, config, {
    enableImplicitConversion: true,
  });
  const errors = validateSync(validatedConfig, {
    skipMissingProperties: false,
  });

  if (errors.length > 0) {
    throw new Error(errors.toString());
  }
  return validatedConfig;
};
