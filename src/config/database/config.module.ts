import { Global, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { DatabaseConfigService } from './config.service';
import configuration from './configuration';
import validate from './validation';

@Global()
@Module({
  imports: [
    ConfigModule.forRoot({
      cache: true,
      validate: validate,
      load: [configuration],
    }),
  ],
  providers: [DatabaseConfigService],
  exports: [DatabaseConfigService],
})
export class DatabaseConfigModule {}
