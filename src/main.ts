import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { AppConfigService } from './config/app/config.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });
  const appConfig = app.get(AppConfigService);
  const port = appConfig.port;
  await app.listen(port);
}
bootstrap();
