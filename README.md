# TodoBackend - NodeJs - NestJs

Ce projet est une implémentation du [TodoBackend](https://docs.google.com/document/d/1tj_MuyutV6_vzfDpYeEvTuy4n_9ceKPAX8T5lStg2h0/edit), utilisé avec NestJs.

Le projet a été généré avec [Nest CLI](https://docs.nestjs.com/cli/overview).

## Installation

Exécuter la commande suivante pour installer les dépendances :

`$>npm i`

## Build

Exécuter la commande suivante pour "build" le projet, le dossier de sortie est "./dist" :

`$>npm run build`

## Lancement

Exécuter la commande suivante pour lancer l'application :

`$>npm start`

## Tests

Exécuter la commande suivante pour lancer les tests :

`$>npm run test`

Pour lancer les tests en mode "watch", il suffit de préfixer la commande précédente par `:watch` :

`$>npm run test:watch`

Même chose pour lancer le "coverage" du projet en utilisant `:cov` :

`$>npm run test:cov`

## Linting et formatage

Enfin pour "linter" le projet, exécuter la commande suivante :

`$>npm run lint`

Pour formatter le projet, exécuter la commande suivante :

`$>npm run format`
