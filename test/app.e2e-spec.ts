import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import { AppModule } from 'src/app.module';
import * as request from 'supertest';

describe('Integration tests', () => {
  let app: INestApplication;
  const PORT = 4050;

  beforeEach(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.listen(PORT);
  });

  afterEach(async () => {
    await app.close();
  });

  describe('Todos', () => {
    const ENDPOINT = '/todos';

    describe('POST on route /', () => {
      it('should get the created todo', async () => {
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'a title' })
          .expect(201)
          .expect((res) => {
            const uuid = res.body.id;
            expect(uuid).toEqual(expect.any(String));
            expect(res.body).toEqual({
              id: uuid,
              title: 'a title',
              completed: false,
              order: 1,
              url: `http://127.0.0.1:${PORT}/todos/${uuid}`,
            });
          });
      });

      it.each`
        title
        ${''}
        ${'  '}
      `(
        'should get 400 when invalid title',
        async ({ title }: { title: string }) => {
          await request(app.getHttpServer())
            .post(ENDPOINT)
            .send({ title })
            .expect(400, {
              statusCode: 400,
              message: ['title must not be blank'],
              error: 'Bad Request',
            });
        },
      );
    });

    describe('GET on route /', () => {
      it('should get no todo', async () => {
        await request(app.getHttpServer()).get(ENDPOINT).send().expect(200, []);
      });

      it('should get todos', async () => {
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'a title' });
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'another title' });
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'summary' });

        await request(app.getHttpServer())
          .get(ENDPOINT)
          .send()
          .expect(200)
          .expect((res) => {
            const [uuid1, uuid2, uuid3] = res.body.map(
              (item: { id: string }) => item.id,
            );
            expect([uuid1, uuid2, uuid3]).toEqual([
              expect.any(String),
              expect.any(String),
              expect.any(String),
            ]);
            expect(res.body).toEqual([
              {
                id: uuid1,
                title: 'a title',
                completed: false,
                order: 1,
                url: `http://127.0.0.1:${PORT}/todos/${uuid1}`,
              },
              {
                id: uuid2,
                title: 'another title',
                completed: false,
                order: 2,
                url: `http://127.0.0.1:${PORT}/todos/${uuid2}`,
              },
              {
                id: uuid3,
                title: 'summary',
                completed: false,
                order: 3,
                url: `http://127.0.0.1:${PORT}/todos/${uuid3}`,
              },
            ]);
          });
      });
    });

    describe('DELETE on route /', () => {
      it('should delete all todos', async () => {
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'a title' });
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'another title' });
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'summary' });

        let todos = await request(app.getHttpServer())
          .get(ENDPOINT)
          .send()
          .expect(200);
        expect(todos.body).toHaveLength(3);

        await request(app.getHttpServer()).delete(ENDPOINT).send().expect(204);

        todos = await request(app.getHttpServer())
          .get(ENDPOINT)
          .set('Accept', 'application/json')
          .send()
          .expect(200);
        expect(todos.body).toHaveLength(0);
      });

      it('should delete all completed todos', async () => {
        let uuid = '';
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'a title' });
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'another title' })
          .expect((res) => (uuid = res.body.id));
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'summary' });

        await request(app.getHttpServer())
          .patch(`${ENDPOINT}/${uuid}`)
          .send({ completed: true })
          .expect(200)
          .expect((res) => {
            const uuid = res.body.id;
            expect(uuid).toEqual(expect.any(String));
            expect(res.body).toEqual({
              id: uuid,
              title: 'another title',
              completed: true,
              order: 2,
              url: `http://127.0.0.1:${PORT}/todos/${uuid}`,
            });
          });

        await request(app.getHttpServer())
          .get(ENDPOINT)
          .send()
          .expect(200)
          .expect((res) => {
            const [uuid1, uuid2, uuid3] = res.body.map(
              (item: { id: string }) => item.id,
            );
            expect([uuid1, uuid2, uuid3]).toEqual([
              expect.any(String),
              expect.any(String),
              expect.any(String),
            ]);
            expect(res.body).toEqual([
              {
                id: uuid1,
                title: 'a title',
                completed: false,
                order: 1,
                url: `http://127.0.0.1:${PORT}/todos/${uuid1}`,
              },
              {
                id: uuid2,
                title: 'another title',
                completed: true,
                order: 2,
                url: `http://127.0.0.1:${PORT}/todos/${uuid2}`,
              },
              {
                id: uuid3,
                title: 'summary',
                completed: false,
                order: 3,
                url: `http://127.0.0.1:${PORT}/todos/${uuid3}`,
              },
            ]);
          });

        await request(app.getHttpServer())
          .delete(ENDPOINT)
          .query({ completed: true })
          .send()
          .expect(204);

        await request(app.getHttpServer())
          .get(ENDPOINT)
          .send()
          .expect(200)
          .expect((res) => {
            const [uuid1, uuid2] = res.body.map(
              (item: { id: string }) => item.id,
            );
            expect([uuid1, uuid2]).toEqual([
              expect.any(String),
              expect.any(String),
            ]);
            expect(res.body).toEqual([
              {
                id: uuid1,
                title: 'a title',
                completed: false,
                order: 1,
                url: `http://127.0.0.1:${PORT}/todos/${uuid1}`,
              },
              {
                id: uuid2,
                title: 'summary',
                completed: false,
                order: 3,
                url: `http://127.0.0.1:${PORT}/todos/${uuid2}`,
              },
            ]);
          });
      });
    });

    describe('GET on route /:id', () => {
      it('should get todo', async () => {
        let id: string;
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'a title' })
          .expect((res) => (id = res.body.id));

        await request(app.getHttpServer())
          .get(`${ENDPOINT}/${id}`)
          .send()
          .expect(200)
          .expect((res) => {
            const uuid = res.body.id;
            expect(uuid).toEqual(expect.any(String));
            expect(res.body).toEqual({
              id: uuid,
              title: 'a title',
              completed: false,
              order: 1,
              url: `http://127.0.0.1:${PORT}/todos/${uuid}`,
            });
          });
      });

      it('should get 404 when unknow id', async () => {
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'a title' });

        await request(app.getHttpServer())
          .get(`${ENDPOINT}/2`)
          .send()
          .expect(404, { statusCode: 404, message: 'Not Found' });
      });
    });

    describe('PATCH on route /:id', () => {
      it('should patch todo', async () => {
        let id: string;
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'title 1' });
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'title 2' })
          .expect((res) => (id = res.body.id));
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'title 3' });

        // Premier patch
        await request(app.getHttpServer())
          .patch(`${ENDPOINT}/${id}`)
          .send({ title: 'title 2 updated', completed: true, order: 4 })
          .expect(200)
          .expect((res) => {
            const uuid = res.body.id;
            expect(uuid).toEqual(expect.any(String));
            expect(res.body).toEqual({
              id: uuid,
              title: 'title 2 updated',
              completed: true,
              order: 4,
              url: `http://127.0.0.1:${PORT}/todos/${uuid}`,
            });
          });

        await request(app.getHttpServer())
          .get(ENDPOINT)
          .send()
          .expect(200)
          .expect((res) => {
            const [uuid1, uuid2, uuid3] = res.body.map(
              (item: { id: string }) => item.id,
            );
            expect([uuid1, uuid2, uuid3]).toEqual([
              expect.any(String),
              expect.any(String),
              expect.any(String),
            ]);
            expect(res.body).toEqual([
              {
                id: uuid1,
                title: 'title 1',
                completed: false,
                order: 1,
                url: `http://127.0.0.1:${PORT}/todos/${uuid1}`,
              },
              {
                id: uuid2,
                title: 'title 3',
                completed: false,
                order: 3,
                url: `http://127.0.0.1:${PORT}/todos/${uuid2}`,
              },
              {
                id: uuid3,
                title: 'title 2 updated',
                completed: true,
                order: 4,
                url: `http://127.0.0.1:${PORT}/todos/${uuid3}`,
              },
            ]);
          });

        // Second patch
        await request(app.getHttpServer())
          .patch(`${ENDPOINT}/${id}`)
          .send({ order: 2 })
          .expect(200)
          .expect((res) => {
            const uuid = res.body.id;
            expect(uuid).toEqual(expect.any(String));
            expect(res.body).toEqual({
              id: uuid,
              title: 'title 2 updated',
              completed: true,
              order: 2,
              url: `http://127.0.0.1:${PORT}/todos/${uuid}`,
            });
          });

        await request(app.getHttpServer())
          .get(ENDPOINT)
          .send()
          .expect(200)
          .expect((res) => {
            const [uuid1, uuid2, uuid3] = res.body.map(
              (item: { id: string }) => item.id,
            );
            expect([uuid1, uuid2, uuid3]).toEqual([
              expect.any(String),
              expect.any(String),
              expect.any(String),
            ]);
            expect(res.body).toEqual([
              {
                id: uuid1,
                title: 'title 1',
                completed: false,
                order: 1,
                url: `http://127.0.0.1:${PORT}/todos/${uuid1}`,
              },
              {
                id: uuid2,
                title: 'title 2 updated',
                completed: true,
                order: 2,
                url: `http://127.0.0.1:${PORT}/todos/${uuid2}`,
              },
              {
                id: uuid3,
                title: 'title 3',
                completed: false,
                order: 3,
                url: `http://127.0.0.1:${PORT}/todos/${uuid3}`,
              },
            ]);
          });
      });

      it('should get 404 when unknow id', async () => {
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'a title' });

        await request(app.getHttpServer())
          .patch(`${ENDPOINT}/2`)
          .send({ title: 'title updated' })
          .expect(404, { statusCode: 404, message: 'Not Found' });
      });

      it('should get 409 when conflicting order', async () => {
        let id: string;
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'title 1' })
          .expect((res) => (id = res.body.id));
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'title 2' });

        await request(app.getHttpServer())
          .patch(`${ENDPOINT}/${id}`)
          .send({ order: 2 })
          .expect(409, {
            message: 'Conflicting order occurred, order:2 already exists',
            statusCode: 409,
          });
      });

      it('should get 400 when invalid order', async () => {
        let id: string;
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'title 1' })
          .expect((res) => (id = res.body.id));

        await request(app.getHttpServer())
          .patch(`${ENDPOINT}/${id}`)
          .send({ order: -1 })
          .expect(400, {
            statusCode: 400,
            message: ['order must not be less than 1'],
            error: 'Bad Request',
          });
      });

      it('should get 400 when invalid title', async () => {
        let id: string;
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'title 1' })
          .expect((res) => (id = res.body.id));

        await request(app.getHttpServer())
          .patch(`${ENDPOINT}/${id}`)
          .send({ title: '' })
          .expect(400, {
            statusCode: 400,
            message: ['title must not be blank'],
            error: 'Bad Request',
          });
        await request(app.getHttpServer())
          .patch(`${ENDPOINT}/${id}`)
          .send({ title: '  ' })
          .expect(400, {
            statusCode: 400,
            message: ['title must not be blank'],
            error: 'Bad Request',
          });
      });
    });

    describe('PUT on route /:id', () => {
      it('should update todo', async () => {
        let id: string;
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'title 1' });
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'title 2' })
          .expect((res) => (id = res.body.id));
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'title 3' });

        // Premier patch
        await request(app.getHttpServer())
          .put(`${ENDPOINT}/${id}`)
          .send({ title: 'title 2 updated', completed: true, order: 4 })
          .expect(200)
          .expect((res) => {
            const uuid = res.body.id;
            expect(uuid).toEqual(expect.any(String));
            expect(res.body).toEqual({
              id: uuid,
              title: 'title 2 updated',
              completed: true,
              order: 4,
              url: `http://127.0.0.1:${PORT}/todos/${uuid}`,
            });
          });

        await request(app.getHttpServer())
          .get(ENDPOINT)
          .send()
          .expect(200)
          .expect((res) => {
            const [uuid1, uuid2, uuid3] = res.body.map(
              (item: { id: string }) => item.id,
            );
            expect([uuid1, uuid2, uuid3]).toEqual([
              expect.any(String),
              expect.any(String),
              expect.any(String),
            ]);
            expect(res.body).toEqual([
              {
                id: uuid1,
                title: 'title 1',
                completed: false,
                order: 1,
                url: `http://127.0.0.1:${PORT}/todos/${uuid1}`,
              },
              {
                id: uuid2,
                title: 'title 3',
                completed: false,
                order: 3,
                url: `http://127.0.0.1:${PORT}/todos/${uuid2}`,
              },
              {
                id: uuid3,
                title: 'title 2 updated',
                completed: true,
                order: 4,
                url: `http://127.0.0.1:${PORT}/todos/${uuid3}`,
              },
            ]);
          });

        // Second patch
        await request(app.getHttpServer())
          .put(`${ENDPOINT}/${id}`)
          .send({ title: 'title 2 re-updated', completed: false, order: 2 })
          .expect(200)
          .expect((res) => {
            const uuid = res.body.id;
            expect(uuid).toEqual(expect.any(String));
            expect(res.body).toEqual({
              id: uuid,
              title: 'title 2 re-updated',
              completed: false,
              order: 2,
              url: `http://127.0.0.1:${PORT}/todos/${uuid}`,
            });
          });

        await request(app.getHttpServer())
          .get(ENDPOINT)
          .send()
          .expect(200)
          .expect((res) => {
            const [uuid1, uuid2, uuid3] = res.body.map(
              (item: { id: string }) => item.id,
            );
            expect([uuid1, uuid2, uuid3]).toEqual([
              expect.any(String),
              expect.any(String),
              expect.any(String),
            ]);
            expect(res.body).toEqual([
              {
                id: uuid1,
                title: 'title 1',
                completed: false,
                order: 1,
                url: `http://127.0.0.1:${PORT}/todos/${uuid1}`,
              },
              {
                id: uuid2,
                title: 'title 2 re-updated',
                completed: false,
                order: 2,
                url: `http://127.0.0.1:${PORT}/todos/${uuid2}`,
              },
              {
                id: uuid3,
                title: 'title 3',
                completed: false,
                order: 3,
                url: `http://127.0.0.1:${PORT}/todos/${uuid3}`,
              },
            ]);
          });
      });

      it('should get 404 when unknow id', async () => {
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'a title' });

        await request(app.getHttpServer())
          .put(`${ENDPOINT}/2`)
          .send({ title: 'title updated', completed: true, order: 4 })
          .expect(404, { statusCode: 404, message: 'Not Found' });
      });

      it('should get 409 when conflicting order', async () => {
        let id: string;
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'title 1' })
          .expect((res) => (id = res.body.id));
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'title 2' });

        await request(app.getHttpServer())
          .put(`${ENDPOINT}/${id}`)
          .send({ title: 'title updated', completed: true, order: 2 })
          .expect(409, {
            message: 'Conflicting order occurred, order:2 already exists',
            statusCode: 409,
          });
      });

      it('should get 400 when invalid order', async () => {
        let id: string;
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'title 1' })
          .expect((res) => (id = res.body.id));

        await request(app.getHttpServer())
          .put(`${ENDPOINT}/${id}`)
          .send({ title: 'title updated', completed: true, order: -1 })
          .expect(400, {
            statusCode: 400,
            message: ['order must not be less than 1'],
            error: 'Bad Request',
          });
      });

      it('should get 400 when invalid title', async () => {
        let id: string;
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'title 1' })
          .expect((res) => (id = res.body.id));

        await request(app.getHttpServer())
          .put(`${ENDPOINT}/${id}`)
          .send({ title: '', completed: true, order: 2 })
          .expect(400, {
            statusCode: 400,
            message: ['title must not be blank'],
            error: 'Bad Request',
          });
        await request(app.getHttpServer())
          .put(`${ENDPOINT}/${id}`)
          .send({ title: '  ', completed: true, order: 2 })
          .expect(400, {
            statusCode: 400,
            message: ['title must not be blank'],
            error: 'Bad Request',
          });
      });
    });

    describe('DELETE on route /:id', () => {
      it('should delete todo', async () => {
        let id: string;
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'title 1' })
          .expect((res) => (id = res.body.id));

        await request(app.getHttpServer())
          .delete(`${ENDPOINT}/${id}`)
          .send()
          .expect(204);

        await request(app.getHttpServer()).get('/todos').send().expect(200, []);
      });

      it('should get 404 when unknow id', async () => {
        await request(app.getHttpServer())
          .post(ENDPOINT)
          .send({ title: 'a title' });

        await request(app.getHttpServer())
          .delete(`${ENDPOINT}/2`)
          .send({ title: 'title updated' })
          .expect(404, {
            statusCode: 404,
            message: 'Not Found',
          });
      });
    });
  });
});
